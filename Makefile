ifndef main_make
main_make := 1

TOP   := $(dir $(firstword $(sort $(realpath $(MAKEFILE_LIST)))))

ifndef CC
CC    := g++
endif
LD    := g++
AR    := ar rcs
EXELD := $(LD)

include $(TOP)/local.mk

DEFAULT_DEPENDS := core

INCLUDES        += $(RTE_INCLUDE)
INCLUDES        += $(LIBBERT_INCLUDE)
CFLAGS          += -fpic -std=gnu++0x -pthread -fvisibility-inlines-hidden -fvisibility=hidden -fms-extensions -mcx16
LIBS            += -lpthread

ifdef MALLOC
ifeq ($(MALLOC),"RTE")
    CFLAGS      += -DMALLOC=1
else
ifeq ($(MALLOC),"STD")
    CFLAGS      += -DMALLOC=2
else
# default to use RTE malloc
    CFLAGS      += -DMALLOC=1
endif
endif
endif

ifdef GRAMMARNAZI
    CFLAGS      += -Wall -Werror -Wno-error=unused-function -Wno-error=strict-aliasing
endif

ifdef HASHBITS
    CFLAGS      += -DHASHBITS=$(HASHBITS)
endif

ifdef DEBUG
    CFLAGS  += -DDEBUG=$(DEBUG) -O0 -g3
else
    CFLAGS  += -O3 -g0 -flto
endif

CFLAGS          += 
#! -Wno-pmf-conversions 

########################################
#

ifndef SYNC_CMD
SYNC_CMD      = hg
PULL_OPTS     = pull --update
PUSH_OPTS     = push
SYNC_URL      = 
endif

PROJECT_DIR := $(TOP)
BUILD_DIR   := $(PROJECT_DIR)build/
SOURCES_DIR := $(PROJECT_DIR)source/

ifndef DEBUG
	OBJ_DIR := $(BUILD_DIR)release/
else
	OBJ_DIR := $(BUILD_DIR)debug/
endif

ifndef NO_ANNOUNCES
	say		:= echo
else
	say		:= true
endif

#########################################
# Service functions:
# subdirs dirlist -> list of all subdirectories of dirlist
# objname sourcefile -> name of corresponding object file
#
rwildcard = $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2) $(filter $(subst *,%,$2),$d))
rfiles    = $(foreach d,$(wildcard $1*),$(call rfiles,$d/,$2) $(filter %/$2,$d))
subdirs   = $(if $(1),$(1) $(call subdirs,$(filter %/,$(wildcard $(addsuffix */,$(1))))),)
objname   = $(OBJ_DIR)objs/$(subst /,_,$(subst $(SOURCES_DIR),,$(basename $(1))))
sources   = $(call rwildcard,$(1),*.c *.cc *.cpp)
incfiles  = $(call rwildcard,$(1),*.h)
incdirs   = $(foreach m, $(sort $(foreach m, $(incfiles),$(if $(findstring control, $$m),,$(dir $(realpath $m))))),-I$m)
module_sources = $(wildcard $(addsuffix *.c*,$(call subdirs,$(addsuffix src/,$(1)))))
module_tests   = $(wildcard $(addsuffix *.c*,$(call subdirs,$(addsuffix tests/,$(1)))))
mkrblist   = $(foreach item,$1,'$(item)',)
mkinclist  = $(foreach item,$1,-I $(item))
unique     = $(if $(strip $1),$(call unique,$(filter-out $(firstword $1),$1),$2 $(firstword $1)),$2)
libdirtorblist = $(call mkrblist,$(foreach i,$1,$(if $(findstring -L,$i),$(subst -L,,$i),)))
libstorblist   = $(call mkrblist,$(foreach i,$1,$(if $(findstring -l,$i),$(subst -l,,$i),)))
ZMQLIB     = $(realpath $(shell find . -name libzmq.a))
ZMQINC     = $(foreach m, $(sort $(foreach m, $(incfiles),$(if $(findstring zmq.h, $m),$(dir $(realpath $m),)))),-I$m)

INCDIRS = $(foreach m, $(sort $(foreach m, $(incfiles),$(if $(findstring control, $m),,$(dir $(realpath $m))))),-I$m)
#########################################
# CPP -> Object template
# CPP_OBJECT_template module,source
# 
define CPP_OBJECT_template
-include $$(call objname,$(2)).d
$$(call objname,$(2)).o: $(2)
	@$(say) '$(1): c++ $$(subst $(SOURCES_DIR),,$$<)'
	@mkdir -p $$(OBJ_DIR) $$(OBJ_DIR)objs/
	@$$(CC) -pipe -fmessage-length=0 $$(CFLAGS) $$(INCLUDES) -M -MMD -MP -MF"$$(@:%.o=%.d)" -c "$$<" -o "$$@"
$(1)_objs += $$(call objname,$(2)).o
endef

#########################################
# static lib template
# STATIC_LIB_template target
# 
define STATIC_LIB_template
$$($(1)_lib_static): $$($(1)_objs)
	@$(say) "$(1): link static $$@"
	@$$(AR) $$@ $$+
$(1): $$($(1)_lib_static)
endef

#########################################
# shared lib template
# SHARED_LIB_template target
# 
define SHARED_LIB_template
$$($(1)_lib_shared): $$($(1)_objs)
	@$(say) 'Linking shared library: $$@'
	@mkdir -p $(RB_BUILD_PREF)
	@$$(LD) -shared -Wl,-soname="$$@" -o"$$@" $$+ $$($(1)_libs)
$(1): $$($(1)_lib_shared)
endef

#########################################
# executable target template
# EXECUTABLE_template target
# 
define EXECUTABLE_template
$(OBJ_DIR)$(1): $$($(1)_objs)
	@$(say) "Linking executable: $$@"
	@$$(EXELD) -o $$@ $$^ $$(LDFLAGS) -L$$(OBJ_DIR) $$(LIB_DIRS) $$(LIBS)
$(1): $(OBJ_DIR)$(1)
endef

#########################################
# Module TEST application template
# TEST_template module,testname,source
#
define TEST_template
$(OBJ_DIR)$(2): $$($(1)_lib_static)
$(OBJ_DIR)$(2): LIBS += $$($(1)_lib_static) -lcore
$$(eval $$(call CPP_OBJECT_template,$(2),$(3)))
$$(eval $$(call EXECUTABLE_template,$(2)))
$(1): $(OBJ_DIR)$(2)
endef

#########################################
# Main TARGET template
# TARGET_template targetName
# 
define TARGET_template
ifndef $(1)_desc
  $(1)_desc := $(1)
endif

build_$(1): $(1)

$(1) : INCLUDES += $$($(1)_include)
$(1) : INCLUDES += $$(foreach m, $$(sort $$(foreach m, $$(incfiles),$$(if $$(findstring control, $$m),,$$(dir $$(realpath $$m))))),-I$$m)
$(1) : INCLUDES += $$(ZMQINC)
$(1) : CFLAGS   += $$($(1)_cflags)
$(1) : LDFLAGS  += $$($(1)_ldflags)
$(1) : INCLUDES += $$(foreach d,$$(filter-out $(1),$$(DEFAULT_DEPENDS)),-I$$(addsuffix include/,$$($$d_root)))
$(1) : LIBS += $$(ZMQLIB)
$(1) : LIBS += $$(call unique,$$($(1)_libs) $$(foreach l,$$(filter-out $(1),$$(DEFAULT_DEPENDS)),$$($$l_libs)),)

$(1)_src += $$(call sources,$$($(1)_root))
$$(foreach src,$$($(1)_src),$$(eval $$(call CPP_OBJECT_template,$(1),$$(src),$$(call objname,$$(src)))))

ifeq "$$($(1))" "exec"
  $$(eval $$(call EXECUTABLE_template,$(1)))
else
  $(1)_lib_static = $$(OBJ_DIR)lib$(1).a
  $$(eval $$(call STATIC_LIB_template,$(1)))
endif

clean_$(1):
	@rm -f $$($(1)_objs) $$($(1)_objs:%.o=%.d)

build: $(1)
install: install_$(1)

.PHONY: $(1) build_$(1) install_$(1) clean_$(1)
endef

###################################################################
# Magic

CURMOD  := $(firstword $(realpath $(MAKEFILE_LIST)))

ifeq ($(dir $(CURMOD)),$(TOP))
build:
else ifndef __first_mod
ifneq (,$(MODULES))
__first_mod := $(MODULES)
else
__first_mod := $(lastword $(subst /, ,$(dir $(CURMOD))))
MODULES     := $(__first_mod)
endif
ifndef $(__first_mod)_root
$(__first_mod)_root := $(dir $(CURMOD))
endif
b: build_$(__first_mod)
i: install_$(__first_mod)
c: clean_$(__first_mod)
endif

define MOD_MAKE_include
$(subst /,_,$2)__m := $$(words $$(MODULES))
ifneq ($(CURMOD),$(2)Makefile)
include $(2)Makefile
ifeq ($$($(subst /,_,$2)__m),$$(words $$(MODULES)))
MODULES   += $(1)
$(1)_root := $(2)
else
$$(lastword $$(MODULES))_root := $(2)
endif
endif
endef

SORTED_MODULES = $(call unique,$(foreach m,$(MODULES),$($m_depends) $m),)
BUILD_MODULES = $(foreach m,$(SORTED_MODULES),$(if $(findstring $m,$(EXCLUDE_MODULES)),,$(if $($m),$m,)))
EXCLUDED_MODULES = $(foreach m,$(SORTED_MODULES),$(if $(findstring $m,$(EXCLUDE_MODULES)),$(if $($m),$m,),))

$(foreach target,$(call rfiles,$(SOURCES_DIR),Makefile),$(eval $(call MOD_MAKE_include,$(lastword $(subst /, ,$(dir $(target)))),$(dir $(target)))))
$(foreach target,$(BUILD_MODULES),$(eval $(call TARGET_template,$(target))))
$(foreach target,$(EXCLUDED_MODULES),$(eval $(call EXCLUDED_TARGET_template,$(target))))

ifneq (,$(strip $(EXCLUDED_MODULES)))
excluded:
	@$(say) "Excluded modules: $(sort $(EXCLUDED_MODULES))"
build: excluded
endif

# End of magic
###################################################################

generate:
	@echo "Generating metadata..."
	@ruby source/tools/ast_loader.rb $(CC) $(INCDIRS) $(INCLUDES) $(CFLAGS)

ctrl:
	@echo "Control-plane building..."
	@cd control && make

all: generate ctrl build install

clean:
	@rm -rf $(BUILD_DIR)release/
	@rm -rf $(BUILD_DIR)debug/

sync:
	@cd $(TOP); $(SYNC_CMD) $(PULL_OPTS)
	
push:
	@cd $(TOP); $(SYNC_CMD) $(PUSH_OPTS)

.PHONY: all build clean install sync push
endif