LIBEVENT_INCLUDE = -I/opt/libevent/include
LIBEVENT_LIBS = -L/opt/libevent/lib -levent

IPP_INCLUDE   = -I/opt/intel/ipp/include
IPP_LIBS      = -L/opt/intel/ipp/lib/intel64 -lippch

DEBUG         = 1
#HASHBITS     = 22

#! CFLAGS        = -march=corei7 -mcx16 -msahf -mno-movbe -maes -mpclmul -mpopcnt -mno-abm -mno-lwp -mno-fma -mno-fma4 -mno-xop -mno-tbm -mno-avx -msse4.2 -msse4.1 -mno-rdrnd -mno-f16c -mno-fsgsbase -mtune=corei7 -m64
#!-mno-bmi -mno-avx2 -mno-lzcnt -mno-bmi2 --param l1-cache-size=32 --param l1-cache-line-size=64 --param l2-cache-size=20480

#! CFLAGS        += -Wno-literal-suffix

MALLOC        = "STD"
#default malloc to use set to RTE, you can choose between STD, TBB and RTE

EXCLUDE_MODULES =

# rsync sync
SYNC_CMD      = rsync
PULL_OPTS     = -avuz $(SYNC_URL) .
PUSH_OPTS     = -avuz . $(SYNC_URL)

#SYNC_URL      = malinin@192.168.56.101:~/projects/mediaserver/
# mac media server
SYNC_URL      = malinin@10.0.0.108:~/projects/mediaserver/
# win media server
#SYNC_URL      = malinin@10.0.0.67:~/projects/mediaserver/
#SYNC_URL      = malinin@192.168.1.104:~/projects/mediaserver/
