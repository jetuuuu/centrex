#ifndef _CMS_TIME_H
#define _CMS_TIME_H

#include <sys/time.h>

static void add_ms_to_timeval(struct timeval *t, uint32_t interval_ms) {
    t->tv_sec = t->tv_sec + (interval_ms / 1000);
    t->tv_usec = t->tv_usec + ((interval_ms % 1000) * 1000);
    if (t->tv_usec > 1000000) {
        t->tv_usec -= 1000000;
        t->tv_sec++;
    }
}

static int compare_timeval(struct timeval *a, struct timeval *b) {
    if (a->tv_sec > b->tv_sec) {
        return 1;
    } else if (a->tv_sec < b->tv_sec) {
        return -1;
    } else if (a->tv_usec > b->tv_usec) {
        return 1;
    } else if (a->tv_usec < b->tv_usec) {
        return -1;
    }
    return 0;
}

#endif // _CMS_TIME_H