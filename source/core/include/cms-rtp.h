#ifndef _CMS_RTP_H
#define _CMS_RTP_H

#define _PACK __attribute__ ((packed))

/* RTP Header (http://tools.ietf.org/html/rfc3550#section-5.1) */
typedef struct rtp_header_t {
#if __BYTE_ORDER == __BIG_ENDIAN
	uint16_t version:2;
	uint16_t padding:1;
	uint16_t extension:1;
	uint16_t csrccount:4;
	uint16_t markerbit:1;
	uint16_t type:7;
#elif __BYTE_ORDER == __LITTLE_ENDIAN
	uint16_t csrccount:4;
	uint16_t extension:1;
	uint16_t padding:1;
	uint16_t version:2;
	uint16_t type:7;
	uint16_t markerbit:1;
#endif
	uint16_t seq_number;
	uint32_t timestamp;
	uint32_t ssrc;
	//uint32_t csrc[16];
} _PACK rtp_header_t;

// RFC 2833
typedef struct dtmf_event_t {
	uint8_t  event;
	uint8_t  volume:6;
	uint8_t  r:1;
	uint8_t  e:1;
	uint16_t duration;
} _PACK dtmf_event_t;


#endif // _CMS_RTP_H