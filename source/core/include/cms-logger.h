#ifndef _CMS_LOGGER_H
#define _CMS_LOGGER_H

#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h> 
#include <fcntl.h>
#include <sys/time.h>
#include <pthread.h>
#include <unistd.h>

#include "cms.h"
#include "cms-obj-pool.h"
#include "cms-queue.h"

struct logger;
#define LOG_INIT logger::instance()
#define LOG_DESTROY logger::destroy()

#define LOG_DEBUG(format, ...) logger::instance()->add_msg(format, __VA_ARGS__)
#define LOG_DEBUG_STR(cstr) /*fprintf(stderr, cstr);*/ LOG_DEBUG("%s", cstr)

static const int16_t MAX_MSG_LEN = 255;
struct log_msg_t {
	char msg[MAX_MSG_LEN];
	int16_t len;
	log_msg_t() : len(0) {}
};

typedef memory_object_pool<log_msg_t> log_msg_pool;
typedef lf_queue<log_msg_t*> log_msg_queue;

enum logger_state_t {
    LOGGER_INACTIVE,
    LOGGER_ACTIVE,
    LOGGER_SHUTTING_DOWN
};

struct logger {
public:
	static logger* instance() {
		if (s_logger != NULL) {
			return s_logger;
		}
		s_logger = new logger();
		return s_logger;
	}

	static void destroy() {
		if (s_logger == NULL) {
			return;
		}
		delete s_logger;
		s_logger = NULL;
	}

	class thread : public std::thread {
    public:
        thread(logger* logger) : 
            std::thread(&logger::thread::run, this),
            m_logger(logger) {}

        int run() {
            do {
                while (!m_logger->m_msg_queue->empty()) {
                    log_msg_t* msg = NULL;
                    if (!m_logger->m_msg_queue->dequeue(&msg)) {
                        continue;
                    }
                    if (msg == NULL) {
                        continue;
                    }

                    ssize_t len = write(m_logger->m_file_d, msg->msg, msg->len);
                    len = write(m_logger->m_file_d, "\n", 1);
            
                    m_logger->m_msg_pool->push(msg);
                }
                fsync(m_logger->m_file_d);
                sched_yield();
                sleep(1);
            } while (likely(m_logger->m_state == LOGGER_ACTIVE));
            return 0;
        }
    private:
        logger* const m_logger;
    };

public:
    logger(const logger&) = delete;
    logger& operator=(const logger&) = delete;

	logger() : m_state(LOGGER_ACTIVE), m_file_d(-1) {
		mzones_list msg_pool_mzones;
        rte_memzone_t msg_pool_mzone;
        msg_pool_mzone.length = 1024 * 1024 * sizeof(uintptr_t);
        msg_pool_mzone.addr = (uintptr_t)malloc(msg_pool_mzone.length);
        msg_pool_mzones.push_back(msg_pool_mzone);
        m_msg_pool = log_msg_pool::instance(msg_pool_mzones);
        if (m_msg_pool == NULL) {
            printf("log_msg_pool is NULL\n");
            throw;
        }

        mzones_list msg_queue_mzones;
        rte_memzone_t msg_queue_mzone;
        msg_queue_mzone.length = 1024 * 1024 * sizeof(uintptr_t);
        msg_queue_mzone.addr = (uintptr_t)malloc(msg_queue_mzone.length);
        msg_queue_mzones.push_back(msg_queue_mzone);
        m_msg_queue = new log_msg_queue(msg_queue_mzones);
        if (m_msg_queue == NULL) {
            printf("m_msg_queue is NULL\n");
            throw;
        }


        m_file = "mediagateway.log";
    	m_file_d = open(m_file.c_str(), O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR);
    	if(m_file_d == -1) {
        	m_file_d = open(m_file.c_str(), O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR);
        	if (m_file_d == -1) {
        		printf("can't create log file\n");
        		throw;
        	}
    	}

        m_th = new logger::thread(this);
		cpu_set_t set;
        CPU_ZERO(&set);
        CPU_SET(0, &set);
        pthread_setaffinity_np(m_th->native_handle(), sizeof(cpu_set_t), &set);
    }
	~logger() {
        m_state = LOGGER_SHUTTING_DOWN;
        m_th->join();
        delete m_th;
        delete m_msg_queue;
        delete m_msg_pool;
        if (m_file_d != -1) {
        	fsync(m_file_d);
        	close(m_file_d);
        	m_file_d = -1;
        }
    }

    bool set_file(const std::string& file) {
    	m_file = file;
    	return true;
    }

    const std::string& get_file() const {
    	return m_file;
    }

    void add_msg(const char* format, ...) {
    	struct timeval curr_time;
    	gettimeofday(&curr_time, NULL);
  		struct tm * timeinfo;
  		time_t rawtime;
  		time(&rawtime);
  		timeinfo = gmtime(&rawtime);

    	log_msg_t* msg = m_msg_pool->pull_zero();  	
    	size_t time_len = strftime(msg->msg, MAX_MSG_LEN - 1, "%F %X.", timeinfo);
    	int header_len = snprintf(msg->msg + time_len, MAX_MSG_LEN - 1 - time_len, 
    		// "%03ld [%lx] ", curr_time.tv_usec / 1000, pthread_self()); 
    		"%03ld ", curr_time.tv_usec / 1000); 
		header_len += time_len;
    	va_list ap;
  		va_start(ap, format);
  		int max_len = MAX_MSG_LEN - header_len;
    	int len  = vsnprintf(msg->msg + header_len, max_len, format, ap);
    	va_end(ap);
    	len = max_len >= len ? len : max_len;
    	msg->len = len + header_len;

        fprintf(stderr, "%s\n", msg->msg);
    	m_msg_queue->enqueue(msg);
    }

public:
	static logger* s_logger;
    volatile logger_state_t m_state;

    log_msg_pool* m_msg_pool;
    log_msg_queue* m_msg_queue;

    int m_file_d;
    std::string m_file;

private:
	logger::thread* m_th;
};

#endif // _CMS_LOGGER_H
