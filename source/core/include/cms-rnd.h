#ifndef _CMS_RND_H
#define _CMS_RND_H

/* RFC-1889, RFC-1321 */
#include <sys/types.h>
#include <sys/time.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
#include <sys/utsname.h>

#include <openssl/md5.h>

static uint32_t md_32(char *string, int length)	{
	MD5_CTX context;
	union {
		char c[16];
		uint32_t x[4];
	} digest;
	MD5_Init(&context);
	MD5_Update(&context, string, length);
	MD5_Final((unsigned char *)&digest, &context);
	uint32_t r = 0;
	for (int i = 0; i < 3; i++) {
		r ^= digest.x[i];
	}
	return r;
}

static uint32_t random_uint32(int type) {
	struct {
		int type;
		struct timeval tv;
		clock_t cpu;
		pid_t pid;
		u_long hid;
		uid_t uid;
		gid_t gid;
		struct utsname name;
	} s;

	gettimeofday(&s.tv, 0);
	uname(&s.name);
	s.type = type;
	s.cpu = clock();
	s.pid = getpid();
	s.hid = gethostid();
	s.uid = getuid();
	s.gid = getgid();
	return md_32((char *)&s, sizeof(s));
}

#endif // _CMS_RND_H