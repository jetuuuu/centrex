#ifndef _CMS_OBJ_POOL_H
#define _CMS_OBJ_POOL_H

#include <assert.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <limits.h>
#include <memory>
#include <list>
#include <iterator>

#include <cms.h>
// #include <cms-logger.h>

struct object_pool_t {};

struct rte_memzone_t {
	uintptr_t addr;
    size_t length;
};

typedef std::list<rte_memzone_t> mzones_list;

#define _padding_size(size, align) ((align) - ((size) % (align)))
#define _size_with_padding(size, align) ((size) + _padding_size(size, align))

inline unsigned optimize_size(unsigned obj_size) {
    return obj_size;
}

template<class _t>
class object_pool : object_pool_t {
public:
    static const size_t type_size = sizeof(_t);
    static_assert(type_size >= sizeof(uintptr_t), "class too short");

    object_pool(const object_pool&) = delete;
    object_pool& operator=(const object_pool&) = delete;

    typedef uintptr_t (*make_new_method)(object_pool<_t>*);

    union edge_node {
        volatile uint128_t value;
        struct {
            volatile uintptr_t ptr;
            volatile uint64_t  tag;
        };
    };

    volatile int m_lock;
    volatile edge_node head;
    volatile edge_node tail;
    volatile size_t pulls;
    volatile size_t pushes;
    const size_t block_size;
    const size_t total_count;
    
    const make_new_method make_new;

    inline uintptr_t __pull_only() {
        edge_node cur_head, next;
        do {
        pull_loop:
            lock();
            cur_head.value = head.value;
            if (unlikely(cur_head.ptr == tail.ptr)) {
                printf("object_pool no free objects!\n");
                unlock();
                return make_new ? make_new(this) : 0;
            }
            next.value = (reinterpret_cast<edge_node*>(cur_head.ptr))->value;
            unlock();
            barrier();
            if (unlikely(!next.ptr)) {
                goto pull_loop;//continue; i have no idea why continue treats by gcc as jmp to cas instruction intead of loop start
            } else if (next.ptr >= 0x8000000000000000) CRASH_ME;
        } while (unlikely(!__sync_bool_compare_and_swap(&head.value, cur_head.value, next.value)));
        __sync_fetch_and_add(&pulls, 1);
        return cur_head.ptr;
    }

    inline uintptr_t __pull_only_unsafe() {
        if (head.ptr == tail.ptr) {
            return make_new ? make_new(this) : 0;
        }
        uintptr_t ret = 0;
        edge_node new_head;
        do {
            new_head.value = (reinterpret_cast<edge_node*>(head.ptr))->value;
        } while (!new_head.ptr);
        ret = head.ptr;
        head.value = new_head.value;
        ++pulls;
        return ret;
    }

    inline void __push_only(const uintptr_t p) {
        (reinterpret_cast<edge_node*>(p))->value = 0;
        edge_node cur_tail, new_tail;
        new_tail.ptr = p;
        do {
            cur_tail.value = tail.value;
            new_tail.tag = cur_tail.tag + 1;
        } while (unlikely(!__sync_bool_compare_and_swap(&tail.value, cur_tail.value, new_tail.value)));
        (reinterpret_cast<edge_node*>(cur_tail.ptr))->tag = new_tail.tag;
        barrier();
        (reinterpret_cast<edge_node*>(cur_tail.ptr))->ptr = new_tail.ptr;
        __sync_fetch_and_add(&pushes, 1);
    }

    inline void __push_only_unsafe(const uintptr_t p) {
        edge_node new_tail;
        (reinterpret_cast<edge_node*>(p))->value = 0;
        new_tail.ptr = p;
        new_tail.tag = tail.tag + 1;
        (reinterpret_cast<edge_node*>(tail.ptr))->tag = new_tail.tag;
        (reinterpret_cast<edge_node*>(tail.ptr))->ptr = new_tail.ptr;
        tail.value = new_tail.value;
        ++pushes;
    }

    inline void __init(const uintptr_t p) {
        (reinterpret_cast<edge_node*>(p))->value = 0;
        tail.ptr = p;
        tail.tag = 0;
        head.ptr = p;
        head.tag = 0;
        ++pushes;
    }

    template<bool zero, typename... _args>
    inline _t* __pull(_args... args) {
        _t* const p = reinterpret_cast<_t*>(__pull_only());
        if(unlikely(!p)) return p;
        if(zero) memset(p, 0, type_size);
        return new(p)_t(args...);
    }

    template<bool zero, typename... _args>
    inline _t* __pull_unsafe(_args... args) {
        _t* const p = reinterpret_cast<_t*>(__pull_only_unsafe());
        if(unlikely(!p)) return p;
        if(zero) memset(p, 0, type_size);
        return new(p)_t(args...);
    }

    object_pool(make_new_method mnew, const size_t size, const size_t cnt) : total_count(cnt), make_new(mnew), block_size(size), pulls(0), pushes(0), m_lock(0) {
    }
public:
    object_pool(const size_t size, const size_t cnt) : total_count(cnt), make_new(NULL), block_size(size), pulls(0), pushes(0), m_lock(0) {
    }

    ~object_pool() {
    }

    template<typename... _args>
    _t* pull(_args... args) {
        return __pull<false, _args...>(args...);
    }

    template<typename... _args>
    _t* pull_unsafe(_args... args) {
        return __pull_unsafe<false, _args...>(args...);
    }

    template<typename... _args>
    _t* pull_zero(_args... args) {
        return __pull<true, _args...>(args...);
    }

    template<typename... _args>
    _t* pull_zero_unsafe(_args... args) {
        return __pull_unsafe<true, _args...>(args...);
    }

    void push(_t* p) {
        p->~_t();
        __push_only(reinterpret_cast<uintptr_t>(p));
    }

    void push_unsafe(_t* p) {
        p->~_t();
        __push_only_unsafe(reinterpret_cast<uintptr_t>(p));
    }

    inline size_t get_count() const {
        return pushes - pulls;
    }

    inline size_t get_block_size() const {
        return block_size;
    }
private:
    inline void lock() {
        while(__sync_lock_test_and_set(&m_lock, 1)) {
            while(m_lock) asm volatile ("rep; nop" ::: "memory");
        }
    }
    inline void unlock() {
        __sync_lock_release(&m_lock);
    }
};

template<class _t, size_t alignment = __CACHE_LINE_ALIGNMENT>
class memory_object_pool : public object_pool<_t> {
protected:
    typedef object_pool<_t> base;
    static_assert(alignment % 8 == 0, "alignment must be divisible by 8");
    static const size_t default_block_size = base::type_size;
    static const size_t memory_pool_block_size  = _size_with_padding(sizeof(memory_object_pool), alignment);

    memory_object_pool(size_t count, size_t bsize) : base(bsize, count) {
    }
public:
    static size_t len_to_count(size_t len, size_t block_size = default_block_size) {
        if (len < memory_pool_block_size) return 0;
        return (len - memory_pool_block_size) / block_size;
    }
    static size_t count_to_len(size_t count, size_t block_size = default_block_size) {
        return memory_pool_block_size + count * block_size;
    }
    static memory_object_pool* instance(mzones_list &zones, size_t unaligned_block_size = default_block_size) {
        if (zones.size() == 0) throw 1;
        size_t block_size = optimize_size(unaligned_block_size);
        size_t count = 0;
        for (auto& z : zones) {
        	//l LOG_DEBUG("default_block_size=%d z.length=%d block_size=%d", (int)default_block_size, (int)z.length, (int)block_size);
            count += len_to_count(z.length, block_size);
        }
        if (!count) return NULL;

        //init pool in first memzone in list
        auto z = zones.begin();
        memory_object_pool* pool = new(reinterpret_cast<void*>((*z).addr))memory_object_pool(count, block_size);
        octet_t* data = reinterpret_cast<octet_t*>(pool) + memory_pool_block_size;
        size_t zone_count = len_to_count((*z).length, block_size);
        for(size_t i = 0; i < zone_count; ++i, data += block_size) {
            if (unlikely(!i)) {
                pool->__init(reinterpret_cast<uintptr_t>(data));
                continue;
            }
            pool->__push_only(reinterpret_cast<uintptr_t>(data));
        }
        //add memory from additional memzones to pool
        ++z;
        for (; z != zones.end(); ++z) {
            octet_t* data = reinterpret_cast<octet_t*>((*z).addr);
            zone_count = len_to_count((*z).length, block_size);
            for(size_t i = 0; i < zone_count; ++i, data += block_size) {
                pool->__push_only(reinterpret_cast<uintptr_t>(data));
            }
        }
        return pool;
    }

    inline size_t get_count() const {
        return base::total_count;
    }
    inline size_t get_free_count() const {
        return base::get_count();
    }
    inline size_t get_used_count() const {
        return get_count() - get_free_count();
    }

    inline size_t get_mem() const {
        return get_count() * base::block_size;
    }
    inline size_t get_free_mem() const {
        return get_free_count() * base::block_size;
    }
    inline size_t get_used_mem() const {
        return get_used_count() * base::block_size;
    }
};

#endif // _CMS_OBJ_POOL_H