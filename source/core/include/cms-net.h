#ifndef _CMS_NET_H
#define _CMS_NET_H

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <net/if.h>
#include <arpa/inet.h>

struct net {
public:
static std::string get_eth_ipv4(const std::string& eth = "eth0") {
 	int fd = socket(AF_INET, SOCK_DGRAM, 0);
 	struct ifreq ifr;
 	ifr.ifr_addr.sa_family = AF_INET;
 	strncpy(ifr.ifr_name, eth.c_str(), IFNAMSIZ - 1);
 	ioctl(fd, SIOCGIFADDR, &ifr);
 	close(fd);
 	return inet_ntoa(((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr);
}
};

#endif // _CMS_NET_H