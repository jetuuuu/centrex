#ifndef _CMS_RTCP_H
#define _CMS_RTCP_H

#include "cms-rtp.h"

/* RTCP Packet Types (http://www.networksorcery.com/enp/protocol/rtcp.htm) */
typedef enum rtcp_type_t : uint8_t {
    RTCP_FIR 	= 192,
    RTCP_SR 	= 200,
    RTCP_RR 	= 201,
    RTCP_SDES 	= 202,
    RTCP_BYE 	= 203,
    RTCP_APP 	= 204,
    RTCP_RTPFB 	= 205,
    RTCP_PSFB 	= 206,
} rtcp_type_t;

/* RTCP Header (http://tools.ietf.org/html/rfc3550#section-6.1) */
typedef struct rtcp_header_t {
#if __BYTE_ORDER == __BIG_ENDIAN
	uint16_t version:2;
	uint16_t padding:1;
	uint16_t rc:5;
	uint16_t type:8;
#elif __BYTE_ORDER == __LITTLE_ENDIAN
	uint16_t rc:5;
	uint16_t padding:1;
	uint16_t version:2;
	uint16_t type:8;
#endif
	uint16_t length:16;
} _PACK rtcp_header_t;

/* RTCP Sender Information (http://tools.ietf.org/html/rfc3550#section-6.4.1) */
typedef struct sender_info_t {
	uint32_t ntp_ts_msw;
	uint32_t ntp_ts_lsw;
	uint32_t rtp_ts;
	uint32_t packets;
	uint32_t octets;
} _PACK sender_info_t;

/* RTCP Report Block (http://tools.ietf.org/html/rfc3550#section-6.4.1) */
typedef struct report_block_t {
	uint32_t ssrc;
	uint32_t flcnpl;
	uint32_t ehsnr;
	uint32_t jitter;
	uint32_t lsr;
	uint32_t delay;
} _PACK report_block_t;

/* RTCP Sender Report (http://tools.ietf.org/html/rfc3550#section-6.4.1) */
typedef struct rtcp_sr_t {
	rtcp_header_t header;
	uint32_t ssrc;
	sender_info_t si;
	report_block_t rb[1];
} _PACK rtcp_sr_t;

/* RTCP SDES (http://tools.ietf.org/html/rfc3550#section-6.5) */
 typedef enum rtcp_sdes_type_t : uint8_t{
 	RTCP_SDES_END   = 0,
    RTCP_SDES_CNAME = 1,
    RTCP_SDES_NAME  = 2,
    RTCP_SDES_EMAIL = 3,
    RTCP_SDES_PHONE = 4,
    RTCP_SDES_LOC   = 5,
    RTCP_SDES_TOOL  = 6,
    RTCP_SDES_NOTE  = 7,
    RTCP_SDES_PRIV  = 8
} rtcp_sdes_type_t;

typedef struct rtcp_sdes_chunk_t {
	uint32_t csrc;
} _PACK rtcp_sdes_chunk_t;

typedef struct rtcp_sdes_item_t {
	uint8_t type;
	uint8_t len;
	char content[1];
} _PACK rtcp_sdes_item_t;

typedef struct rtcp_sdes_t {
	rtcp_header_t header;
	uint32_t ssrc;
	rtcp_sdes_chunk_t chunk;
	rtcp_sdes_item_t item;
} _PACK rtcp_sdes_t;

#endif // _CMS_RTCP_H