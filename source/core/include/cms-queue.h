#ifndef _CMS_QUEUE_H
#define _CMS_QUEUE_H

// #include <cms-logger.h>

// http://haxpath.squarespace.com/imported-20100930232226/2010/8/25/a-lock-free-queue-in-c.html

#include <assert.h>

struct queue_t {};

template <class _t>
class lf_queue : queue_t {
    struct node_t;
    typedef memory_object_pool<node_t> node_pool;
    struct pointer_t {
        node_t *node;
        uint32_t count;
        pointer_t() : node(NULL), count(0) {}
        pointer_t(node_t *node, uint32_t count) {
            this->node = node;
            this->count = count;
        }
        bool equals(pointer_t &other) {
            return node == other.node && count == other.count;
        }
    };

    struct node_t {
        _t value;
        pointer_t next;
        node_t() {
            next.node = NULL;
            next.count = 0;
        }
        node_t(pointer_t next, _t value) {
            this->next = next;
            this->value = value;
        }
    };

    node_pool* nodes;
    pointer_t head;
    pointer_t tail;

    bool compare_and_swap(pointer_t *address, pointer_t *oldValue, pointer_t newValue) {
        return __sync_bool_compare_and_swap((uint64_t*) address, *(uint64_t*) oldValue, *(uint64_t*) &newValue);
    }

public:
    lf_queue(const mzones_list& mzones) {
        assert (sizeof(pointer_t) == sizeof(uint128_t));

        nodes = memory_object_pool<node_t>::instance(const_cast<mzones_list&>(mzones));
        if (nodes == NULL) {
            fprintf(stderr, "Node pool is NULL\n");
            throw 1;
        }

        // printf("queue blocks count=%d free_count=%d used_count=%d\n", (int)nodes->get_count(), (int)nodes->get_free_count(),(int)nodes->get_used_count());
        // printf("memory mem=%d free_mem=%d get_used_mem=%d", (int)nodes->get_mem(), (int)nodes->get_free_mem(), (int)nodes->get_used_mem());

        node_t *node = nodes->pull_zero();
        head.node = node;
        tail.node = node;
    }

    virtual ~lf_queue() {
        nodes->push(head.node);
        delete nodes;
    }

    bool empty() {
        return head.node == tail.node;
    }

    void enqueue(_t value) {
        node_t *node = nodes->pull_zero();
        node->value = value;
        node->next.node = NULL;
        pointer_t tail;
        while (true) {
            tail = this->tail;
            pointer_t next;
            if (tail.node != NULL) {
                next = tail.node->next;
            }
            if (tail.equals(this->tail) == false) {
                continue;
            }
            if (next.node != NULL) {
                compare_and_swap(&this->tail, &tail, pointer_t(next.node, tail.count + 1));
                continue;
            }
            if (compare_and_swap(&tail.node->next, &next, pointer_t(node, next.count + 1))) {
                break;
            }
        }
        compare_and_swap(&this->tail, &tail, pointer_t(node, tail.count + 1));
    }

    bool dequeue(_t *value) {
        pointer_t head;
        while(true) {
            head = this->head;
            pointer_t tail = this->tail;
            pointer_t next = head.node->next;
            if (head.equals(this->head) == false) {
                continue;
            }
            if (head.node == tail.node) {
                if (next.node == NULL) {
                    return false;
                }
                compare_and_swap(&this->tail, &tail, pointer_t(next.node, tail.count + 1));
            } else {
                *value = next.node->value;
                if (compare_and_swap(&this->head, &head, pointer_t(next.node, head.count + 1))) {
                    break;
                }
            }
        }
        nodes->push(head.node);
        // printf("used_count=%d\n", (int)nodes->get_used_count());
        return true;
    }
};

#endif // _CMS_QUEUE_H