#ifndef _CMS_H
#define _CMS_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string>
#include <list>
#include <map>
#include <thread>
#include <condition_variable>
#include <atomic>
#include <chrono>

#define __CACHE_LINE_ALIGNMENT (128)
#define CRASH_ME (*((char*)(1))=0)
typedef __uint128_t uint128_t;
typedef uint8_t octet_t;

#if MALLOC == 1
#include <rte_malloc.h>
#define tcms_malloc(x)    rte_malloc(NULL, x, __CACHE_LINE_ALIGNMENT)
#define tcms_free(x)      rte_free(x)
#elif MALLOC == 2
// #warning "Using STD malloc, better change MALLOC to RTE"
#define tcms_malloc(x)    malloc(x)
#define tcms_free(x)      free(x)
#elif MALLOC == 3
// #warning "Using TBB scalable allocator, better change MALLOC to RTE"
#include <tbb/scalable_allocator.h>
#define tcms_malloc(x)    scalable_aligned_malloc(x, __CACHE_LINE_ALIGNMENT)
#define tcms_free(x)      scalable_aligned_free(x)
#else
#warning "Using default STD malloc, better change MALLOC to RTE"
#define tcms_malloc(x)    malloc(x)
#define tcms_free(x)      free(x)
#endif

#define barrier() __asm__ __volatile__("":::"memory")
#define write_barrier() __asm__ __volatile__("sfence":::"memory")
#define read_barrier() __asm__ __volatile__("lfence":::"memory")

#ifndef likely
#define likely(x)   __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)
#endif

#ifndef likely_ptr
#define likely_ptr(x)   likely((x) == NULL)
#define unlikely_ptr(x) unlikely((x) != NULL)
#endif

#endif // _TCMS_H