#ifndef _TCP_PROCESSING_H
#define _TCP_PROCESSING_H

#include "mediaserver.pb.h"
using namespace mediaserver;

struct connection_ctx {
    engine* e;
    sockaddr_in addr;
};

static void tcp_read_callback(struct bufferevent *bev, void *ctx) {
    struct connection_ctx* conn_ctx = reinterpret_cast<struct connection_ctx*>(ctx);
    if (conn_ctx == NULL) {
        printf("Connection context is NULL\n");
        return;
    }
    engine* en = conn_ctx->e;

    struct sockaddr_in* addr = reinterpret_cast<struct sockaddr_in*>(&conn_ctx->addr);
    // printf("Connection address=%s port=%d\n", inet_ntoa(addr->sin_addr), addr->sin_port);
    
    int MSM_BUFFER_SIZE = 1024 * 1024;
    char* msm_buffer = (char*)tcms_malloc(MSM_BUFFER_SIZE);
    memset(msm_buffer, 0, MSM_BUFFER_SIZE);

    size_t len = bufferevent_read(bev, msm_buffer, MSM_BUFFER_SIZE);
    // printf("read %d bytes\n", (int)len);
    msm_buffer[len] = 0;

    mediaserver::MediaServerRep msm_rep;
    mediaserver::Media* media = new mediaserver::Media();

    struct evbuffer *output = bufferevent_get_output(bev);
    mediaserver::MediaServerReq msm;
    if (!msm.ParseFromString(msm_buffer)) {
        LOG_DEBUG_STR("Error: Can't parse data!");
        msm_rep.set_res(MediaServerRep_Result::MediaServerRep_Result_InternalServerError);
    } else {
        // LOG_DEBUG("Received: command=%d param1=\"%s\"", (int)msm.command(), msm.id().c_str());
        msm_rep.set_res(MediaServerRep_Result::MediaServerRep_Result_OK);
        switch (msm.command()) {
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateEndPoint: {
                // LOG_DEBUG_STR("CreateEndPoint");
                uint32_t id = en->create_endpoint(msm.media(), output);
                LOG_DEBUG("endpointId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_DestroyObject: {
                // LOG_DEBUG_STR("DestroyObject");
                en->destroy_object(msm.obj().id());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_GetEndPointMedia: {
                // LOG_DEBUG_STR("GetEndPointSDP");
                *media = en->get_endpoint_media(msm.obj().id());
                msm_rep.set_allocated_media(media);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_UpdateEndPointMedia: {
                // LOG_DEBUG_STR("UpdateEndPointSDP");
                en->update_endpoint_media(msm.obj().id(), msm.media());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_SetReceiver: {
                // LOG_DEBUG_STR("SetReceiver");
                if (msm.obj().mg() == en->id()) {
                    en->set_receiver(msm.obj().id(), msm.params(0).obj().id());
                } else {
                    en->set_receiver(msm.obj().id(), msm.params(0).recv().obj().mg(), 
                                                     msm.params(0).recv().obj().id(),
                                                     msm.params(0).recv().addr().ip(),
                                                     msm.params(0).recv().addr().port());
                }
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_SendDTMF: {
                // LOG_DEBUG_STR("SendDTMF");
                en->send_dtmf(msm.obj().id(), msm.params(0).str());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_ResetReceiver: {
                // LOG_DEBUG_STR("ResetReceiver");
                en->reset_receiver(msm.obj().id());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreatePlayer: {
                // LOG_DEBUG_STR("CreatePlayer");
                uint32_t id = en->create_player(msm.params(0).str());
                LOG_DEBUG("playerId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateRecorder: {
                // LOG_DEBUG_STR("CreateRecorder");
                uint32_t id = en->create_recorder(msm.params(0).str());
                LOG_DEBUG("recorderId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateTranscoder: {
                // LOG_DEBUG_STR("CreateTranscoder");
                uint32_t id = en->create_transcoder(msm.params(0).codec(), msm.params(1).codec());
                LOG_DEBUG("transcoderId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateSRTP: {
                // LOG_DEBUG_STR("CreateSRTP");
                uint32_t id = en->create_srtp(msm.params(0).str(), msm.params(1).str() == "1");
                LOG_DEBUG("srtpId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateMixer: {
                // LOG_DEBUG_STR("CreateMixer");
                uint32_t id = en->create_mixer();
                LOG_DEBUG("mixerId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_AttachToMixer: {
                // LOG_DEBUG_STR("AttachToMixer");
                en->attach_to_mixer(msm.obj().id(), msm.params(0).obj().id());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_DetachFromMixer: {
                // LOG_DEBUG_STR("DetachFromMixer");
                en->detach_from_mixer(msm.obj().id(), msm.params(0).obj().id());
                break;
            }

            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateSimpleMixer: {
                // LOG_DEBUG_STR("CreateSimpleMixer");
                uint32_t id = en->create_simple_mixer();
                LOG_DEBUG("simpleMixerId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_AttachStreamToSimpleMixer: {
                // LOG_DEBUG_STR("AttachStreamToSimpleMixer");
                stream_t stream;
                stream.obj.mg = msm.params(0).obj().mg();
                stream.obj.id = msm.params(0).obj().id();
                en->attach_stream_to_simple_mixer(msm.obj().id(), stream, msm.params(1).obj().id());
                break;
            }
            case MediaServerReq_CommandType::MediaServerReq_CommandType_DetachStreamFromSimpleMixer: {
                // LOG_DEBUG_STR("DetachStreamFromSimpleMixer");
                stream_t stream;
                stream.obj.mg = msm.params(0).obj().mg();
                stream.obj.id = msm.params(0).obj().id();
                en->detach_stream_from_simple_mixer(msm.obj().id(), stream);
                break;
            }

            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateEmptyObject: {
                // LOG_DEBUG_STR("CreateEmptyObject");
                uint32_t id = en->create_empty_object();
                LOG_DEBUG("emptyObjectId=%d", id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(id);
                break;
            }

            case MediaServerReq_CommandType::MediaServerReq_CommandType_CreateVideoMix: {
                uint32_t mixer_id = en->create_video_mixer();
                LOG_DEBUG("video mixer id = %d", mixer_id);
                msm_rep.mutable_obj()->set_mg(en->id());
                msm_rep.mutable_obj()->set_id(mixer_id);
                break;
            }

            default: {
                LOG_DEBUG_STR("Unknown command");
                msm_rep.set_res(MediaServerRep_Result::MediaServerRep_Result_NotFound);
            }
        }
    }

    std::string result = msm_rep.SerializeAsString();
    tcms_free(msm_buffer);

    int res = evbuffer_add(output, result.c_str(), (int)result.length()); 
    if (res == -1) {
        fprintf(stderr, "evbuffer_add error\n");
    }
    // LOG_DEBUG("Sending: %d res=%d", (int)result.length(), res);
}

static void tcp_event_callback(struct bufferevent *bev, short events, void *ctx) {
    if (events & BEV_EVENT_ERROR) {
        fprintf(stderr, "Error from bufferevent\n");
    }
    if (events & (BEV_EVENT_EOF | BEV_EVENT_ERROR)) {
        bufferevent_free(bev);
    }
}

static void tcp_accept_connection_callback(struct evconnlistener *listener, evutil_socket_t fd, struct sockaddr *address, int socklen, void *ctx) {
    struct event_base *base = evconnlistener_get_base(listener);
    struct bufferevent *bev = bufferevent_socket_new(base, fd, BEV_OPT_CLOSE_ON_FREE);

    struct connection_ctx* conn_ctx = new connection_ctx();
    conn_ctx->e = reinterpret_cast<engine*>(ctx);
    memcpy(&conn_ctx->addr, address, socklen);

    bufferevent_setcb(bev, tcp_read_callback, NULL, tcp_event_callback, conn_ctx);
    bufferevent_enable(bev, EV_READ|EV_WRITE);
}

static void tcp_accept_error_callback(struct evconnlistener *listener, void *ctx) {
    struct event_base *base = evconnlistener_get_base(listener);
    int err = EVUTIL_SOCKET_ERROR();
    fprintf(stderr, "Got an error %d (%s) on the listener. Shutting down.\n", err, evutil_socket_error_to_string(err));
    event_base_loopexit(base, NULL);
}

#endif // _TCP_PROCESSING_H
