#ifndef _PACKET_BUFFER_H
#define _PACKET_BUFFER_H

#include <thread>

#include <event.h>
#include <event2/listener.h>
#include <event2/bufferevent.h>
#include <event2/buffer.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <sys/time.h>

struct obj_t {
	uint32_t mg;
	uint32_t id;
	uint16_t tbl_oid;
	obj_t() : mg(0), id(0), tbl_oid(0) {}
};

struct receiver_t {
	obj_t obj;
	sockaddr_in addr;
	bool send;
	receiver_t() : send(false) {}
};

struct stream_t {
	obj_t obj;
};

struct packet_t {
	uint8_t data[1500];
	uint16_t len;
	bool rtcp;
	bool promise;
	struct timeval t;
	receiver_t recv;
	stream_t stream;
	packet_t() : len(0), rtcp(false), promise(false) {}
};

struct cluster_packet_t {
	obj_t obj;
	stream_t stream;
	uint16_t data_len;
	uint8_t data[1500];
};

struct cluster_transport_t {
	int rtp_socket_fd; 
	int rtcp_socket_fd;

	struct event rtp_event;
	struct event rtcp_event;
	cluster_transport_t() : rtp_socket_fd(-1), rtcp_socket_fd(-1) {}
};

#include <iostream>
#include <event.h>
#include <sys/socket.h>
#include <fcntl.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>

#include <cms.h>
#include <cms-obj-pool.h>
#include <cms-queue.h>
#include <cms-logger.h>

typedef memory_object_pool<packet_t> packet_pool;
typedef lf_queue<packet_t*> packet_queue;

struct packet_buffer {
public:
	packet_buffer(const packet_buffer&) = delete;
    packet_buffer& operator=(const packet_buffer&) = delete;

	packet_buffer(struct event_base* base, packet_pool* packet_pool, packet_queue* packet_queue) : 
		m_event_base(base),
		m_packet_pool(packet_pool), 
		m_packet_queue(packet_queue) {
		init_cluster_transport();
	}

	~packet_buffer() {
		destroy_cluster_transport();
		LOG_DEBUG_STR("~packet_buffer");
	}

	packet_pool* get_packet_pool() { return m_packet_pool; }
	packet_queue* get_packet_queue() { return m_packet_queue; }
	struct event_base* get_event_base() const {return m_event_base; }

	bool bind_socket_event(const uint16_t& port, int& udpsock_fd, struct event& evt, event_callback_fn evt_fn) {
		if ((udpsock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
			LOG_DEBUG("ERROR errno=%d  - unable to create socket", errno);
			return false;
		}
		int nReqFlags = fcntl(udpsock_fd, F_GETFL, 0);
		if (nReqFlags < 0) {
			LOG_DEBUG_STR("ERROR - cannot set socket options");
			return false;
		}
		if (fcntl(udpsock_fd, F_SETFL, nReqFlags | O_NONBLOCK) < 0) {
			LOG_DEBUG_STR("ERROR - cannot set socket options");
			return false;
		}

		struct sockaddr_in addr;
		memset(&addr, 0, sizeof(struct sockaddr_in));
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons(port);
		addr.sin_family = AF_INET;
		int nOptVal = 1;
		if (setsockopt(udpsock_fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&nOptVal, sizeof(nOptVal))) {
			LOG_DEBUG_STR("ERROR - socketOptions: Error at Setsockopt");
			return false;
		}
		int err = bind(udpsock_fd, (struct sockaddr*)&addr, sizeof(addr)); 
		if (err != 0) {
			LOG_DEBUG("Error: Unable to bind port=%d err=%d", (int)port, err);
			return false;
		}
		event_assign(&evt, get_event_base(), (int)udpsock_fd, EV_READ | EV_PERSIST, evt_fn, this);
		event_add(&evt, NULL);
		return true;
	}

	const cluster_transport_t& get_cluster_transport() const { return m_cluster_transport; }

	static void process_udp_packet_event(evutil_socket_t socket, short int flags, void* buffer, bool rtcp = false) {
		unsigned int unFromAddrLen;
		int nByte = 0;
		struct sockaddr_in stFromAddr;
		unFromAddrLen = sizeof(stFromAddr);

		packet_buffer* pbuffer = (packet_buffer*)buffer;
		packet_pool* ppool = pbuffer->get_packet_pool();
		packet_t* cluster_packet = ppool->pull_zero();
		if ((nByte = recvfrom(socket, cluster_packet->data, sizeof(cluster_packet->data) - 1, 0, (struct sockaddr *)&stFromAddr, &unFromAddrLen)) == -1) {
			LOG_DEBUG_STR("error occured while receiving");
			ppool->push(cluster_packet);
			return;
		}

		if (sizeof(packet_t) < sizeof(cluster_packet_t)) {
			LOG_DEBUG_STR("error: sizeof(packet_t) < sizeof(cluster_packet_t)");
			ppool->push(cluster_packet);
			return;
		}

		/* TODO: доделать кластерную передачу
		cluster_packet_t cpacket;
		memcpy(&cpacket, cluster_packet->data, nByte);

		packet_t* packet = ppool->pull_zero();
		packet->len = cpacket.udp_len;
		memcpy(&packet->data, &cpacket.data, cpacket.udp_len);
		packet->recv.recv_len = cpacket.recv_len;
		memcpy(&packet->recv.receiver, &cpacket.data[cpacket.udp_len], cpacket.recv_len);
		packet->rtcp = rtcp;

		packet_queue* pqueue = pbuffer->get_packet_queue();
		pqueue->enqueue(packet);
		ppool->push(cluster_packet);
		*/
	}

	static void rtp_packet_event(evutil_socket_t socket, short int flags, void* buffer) {
		process_udp_packet_event(socket, flags, buffer);
	}

	static void rtcp_packet_event(evutil_socket_t socket, short int flags, void* buffer) {
		process_udp_packet_event(socket, flags, buffer, true);
	}
private:
	void init_cluster_transport() {
		if (!bind_socket_event(20000, m_cluster_transport.rtp_socket_fd, m_cluster_transport.rtp_event, (event_callback_fn)&packet_buffer::rtp_packet_event)) {
			LOG_DEBUG_STR("ERROR - init_cluster_transport: can't bind rtp udp port");
		}
		if (!bind_socket_event(20001, m_cluster_transport.rtcp_socket_fd, m_cluster_transport.rtcp_event, (event_callback_fn)&packet_buffer::rtcp_packet_event)) {
			LOG_DEBUG_STR("ERROR - init_cluster_transport: can't bind rtcp udp port");
		}
	}
	void destroy_cluster_transport() {
		event_del(&m_cluster_transport.rtp_event);
    	event_del(&m_cluster_transport.rtcp_event);

    	if (m_cluster_transport.rtp_socket_fd != -1) {
    		close(m_cluster_transport.rtp_socket_fd);
    	}
    	if (m_cluster_transport.rtcp_socket_fd != -1) {
    		close(m_cluster_transport.rtcp_socket_fd);
    	}
	}

private:
	packet_pool* m_packet_pool;
	packet_queue* m_packet_queue;

	struct event_base* m_event_base;
	cluster_transport_t m_cluster_transport;
};

#endif // _PACKET_BUFFER_H