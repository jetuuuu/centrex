#ifndef _ENDPOINT_H
#define _ENDPOINT_H

#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include <sstream>

#include <mediaserver.pb.h>

#include <iostream>
#include <sys/socket.h>
#include <fcntl.h>

#include <packet-buffer.h>
#include <cms-rtp.h>
#include <cms-rtcp.h>
#include <cms-net.h>
#include <cms-logger.h>

#include <codec-consts.h>

#include <media-obj.h>
struct endpoint : media_object_t {
public:
	static const uint16_t START_PORT = 60000;
	static const uint16_t END_PORT = 65534;
	static volatile uint16_t s_next_port;

	static uint16_t get_next_port() {
		__sync_bool_compare_and_swap(&s_next_port, END_PORT, START_PORT);
		return __sync_fetch_and_add(&s_next_port, 2);
	}

	static void rtp_packet_event(evutil_socket_t socket, short int flags, void* ep) {
		unsigned int unFromAddrLen;
		int nByte = 0;
		struct sockaddr_in stFromAddr;

		unFromAddrLen = sizeof(stFromAddr);

		endpoint* e = reinterpret_cast<endpoint*>(ep);
		e->set_remote_rtp_socket(socket);
		packet_pool* ppool = e->get_packet_buffer()->get_packet_pool();
		packet_t* packet = ppool->pull_zero();
		if ((nByte = recvfrom(socket, packet->data, sizeof(packet->data) - 1, 0, (struct sockaddr *)&stFromAddr, &unFromAddrLen)) == -1) {
			LOG_DEBUG_STR("error occured while receiving");
			ppool->push(packet);
			return;
		}
        
		e->set_remote_rtp_socket(socket);
		e->set_remote_rtp_addr(stFromAddr);
		packet->len = nByte;
		// printf("packet in: %d:%d %s:%d len=%d\n", e->mg(), e->id(), inet_ntoa(stFromAddr.sin_addr), ntohs(stFromAddr.sin_port), (int)packet->len);
		
        receiver_t* recv = e->receiver();
		if (recv != NULL) {
			memcpy(&(packet->recv), (void*)recv, sizeof(receiver_t));
			// printf("init receiver: %s len=%d\n", packet->recv.receiver, (int)packet->recv.recv_len);
		}

        packet->stream.obj.mg = e->mg();
		packet->stream.obj.id = e->id();

		packet_queue* pqueue = e->get_packet_buffer()->get_packet_queue();

		/* Внутренности RTP пакета */
        /*
		printf("RTP sin_port=%d sin_addr=%s\n", ntohs(stFromAddr.sin_port), inet_ntoa(stFromAddr.sin_addr));
		print_rtp_info(packet);
        */

		if (!e->detect_dtmf(packet) && recv != NULL) {
			pqueue->enqueue(packet);
		} else {
			ppool->push(packet);
			return;
		}
	}

	static void print_rtp_info(packet_t* packet) {
		struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);
		printf("UDP packet len=%d\n", packet->len);
		printf("RTP Version:%x Padding:%d Extension:%d csrccount=%d markerbit=%d type=%d\n", rtp->version, rtp->padding, rtp->extension, 
			                                                                               rtp->csrccount, rtp->markerbit, rtp->type);
		printf("RTP seq_number=%x timestamp=%d ssrc=%x\n", ntohs(rtp->seq_number), ntohl(rtp->timestamp), ntohl(rtp->ssrc));
		printf("RTP Payload len=%d\n", (int16_t)(packet->len - sizeof(rtp_header_t)));
	}

	static void rtcp_packet_event(evutil_socket_t socket, short int flags, void* ep) {
		unsigned int unFromAddrLen;
		int nByte = 0;
		struct sockaddr_in stFromAddr;

		unFromAddrLen = sizeof(stFromAddr);

		endpoint* e = (endpoint*)ep;

		e->set_remote_rtcp_socket(socket);
		packet_pool* ppool = e->get_packet_buffer()->get_packet_pool();
		packet_t* packet = ppool->pull_zero();
		if ((nByte = recvfrom(socket, packet->data, sizeof(packet->data) - 1, 0, (struct sockaddr *)&stFromAddr, &unFromAddrLen)) == -1) {
			LOG_DEBUG_STR("error occured while receiving");
			ppool->push(packet);
			return;
		}

		e->set_remote_rtcp_socket(socket);
		e->set_remote_rtcp_addr(stFromAddr);
		packet->len = nByte;
		packet->rtcp = true;

        receiver_t* recv = e->receiver();
		if (recv != NULL) {
			memcpy(&(packet->recv), (void*)recv, sizeof(receiver_t));
			// printf("packet receiver: %s len=%d\n", packet->recv.receiver, (int)packet->recv.recv_len);
		}

        packet->stream.obj.mg = e->mg();
		packet->stream.obj.id = e->id();

		packet_queue* pqueue = e->get_packet_buffer()->get_packet_queue();

		/* Внутренности RTCP пакета */
		// printf("RTCP sin_port=%d sin_addr=%s\n", ntohs(stFromAddr.sin_port), inet_ntoa(stFromAddr.sin_addr));
		// print_rtcp_info(packet);

        if (recv == NULL) {
            ppool->push(packet);
            return;
        }
		pqueue->enqueue(packet);
	}

	static void print_rtcp_info(packet_t* packet) {
		printf("UDP packet len=%d\n", packet->len);
		struct rtcp_header_t* rtcp = reinterpret_cast<struct rtcp_header_t*>(&packet->data);
		printf("RTCP version:%x padding:%d rc:%d type=%d\n", rtcp->version, rtcp->padding, rtcp->rc, rtcp->type);
		printf("RTCP len=%d\n", ntohs(rtcp->length));
		switch (rtcp->type) {
			case RTCP_SR: {
				struct rtcp_sr_t* rtcp_sr = reinterpret_cast<struct rtcp_sr_t*>(&packet->data);
				printf("RTCP-SR sizeof(rtcp_sr_t)=%d\n", (int)sizeof(rtcp_sr_t));
				printf("RTCP-SR ssrc=%x\n", ntohl(rtcp_sr->ssrc));
				printf("RTCP-SR ntp_ts_msw=%x ntp_ts_lsw=%x rtp_ts=%x\n", ntohl(rtcp_sr->si.ntp_ts_msw), ntohl(rtcp_sr->si.ntp_ts_lsw), ntohl(rtcp_sr->si.rtp_ts));
				printf("RTCP-SR packets=%u octets=%u\n", ntohl(rtcp_sr->si.packets), ntohl(rtcp_sr->si.octets));
				printf("RTCP-SR Report Block: ssrc=%x flcnpl=%u ehsnr=%u jitter=%u lsr=%u delay=%u\n", 
					ntohl(rtcp_sr->rb[0].ssrc), ntohl(rtcp_sr->rb[0].flcnpl), ntohl(rtcp_sr->rb[0].ehsnr),
					ntohl(rtcp_sr->rb[0].jitter), ntohl(rtcp_sr->rb[0].lsr), ntohl(rtcp_sr->rb[0].delay));

				if (packet->len > sizeof(rtcp_sr_t)) {
					struct rtcp_header_t* rtcp_1 = reinterpret_cast<struct rtcp_header_t*>(packet->data + sizeof(rtcp_sr_t));
					printf("RTCP_1 version:%x padding:%d rc:%d type=%d\n", rtcp_1->version, rtcp_1->padding, rtcp_1->rc, rtcp_1->type);
					printf("RTCP_1 len=%d\n", ntohs(rtcp_1->length));
					switch(rtcp_1->type) {
						case RTCP_SDES: { 
							struct rtcp_sdes_t* rtcp_sdes = reinterpret_cast<struct rtcp_sdes_t*>(packet->data + sizeof(rtcp_sr_t));
							printf("RTCP-SDES version:%x padding:%d rc:%d type=%d\n", rtcp_sdes->header.version, rtcp_sdes->header.padding, rtcp_sdes->header.rc, rtcp_sdes->header.type);
							printf("RTCP-SDES ssrc=%x csrc=%x\n", ntohl(rtcp_sdes->ssrc), ntohl(rtcp_sdes->chunk.csrc));
							printf("RTCP-SDES Item: type=%d len=%d content=%d\n", rtcp_sdes->item.type, rtcp_sdes->item.len, rtcp_sdes->item.content[0]);
							break;
						}
						default:
							printf("Unknown RTCP_1 type\n");

					};
				}
				break;
			}
			default:
				printf("Unknown RTCP type\n");
		};
	}

    endpoint(const uint32_t& mg, packet_buffer* buffer, const mediaserver::Media& remote_media, struct evbuffer* output) : 
    	media_object_t(mg, buffer),
    	m_remote_media(remote_media),
    	m_rtp_udpsock_fd(-1),
		m_rtcp_udpsock_fd(-1),
    	m_remote_rtp_socket(0),
    	m_remote_rtcp_socket(0),
    	m_first_packet(true),
    	m_output(output),
    	m_dtmf_marker(false) {
    	update_remote_sockets();
    	printf("media=%s\n", remote_media.ip().c_str());
    	init_dtmf_pqueue();
    }
    virtual ~endpoint() {
    	if (!m_local_media.ip().empty()) {
    		event_del(&m_rtp_event);
    		event_del(&m_rtcp_event);
    	}

    	if (m_rtcp_udpsock_fd != -1) {
    		close(m_rtcp_udpsock_fd);
    	}
    	if (m_rtp_udpsock_fd != -1) {
    		close(m_rtp_udpsock_fd);
    	}

    	delete_dtmf_pqueue();
    }

    virtual bool process_packet(packet_t* packet) {
    	// printf("endpoint %s process packet\n", id().c_str());
    	if (packet->len == 0) {
    		return false;
    	}

        if (!packet->rtcp) {
            if (!m_dtmf_out_pqueue->empty()) {
                packet_t* dtmf_packet = NULL;
                if (m_dtmf_out_pqueue->dequeue(&dtmf_packet)) {
                    packet->len = dtmf_packet->len;

                    struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);
                    struct rtp_header_t* rtp_dtmf = reinterpret_cast<struct rtp_header_t*>(&dtmf_packet->data);
                    struct dtmf_event_t* dtmf_ev = reinterpret_cast<struct dtmf_event_t*>(&dtmf_packet->data[sizeof(rtp_header_t)]);
                    rtp->markerbit = rtp_dtmf->markerbit;
                    rtp->type = rtp_dtmf->type;
                    // rtp->timestamp = htonl(ntohl(rtp->timestamp) - ntohs(dtmf_ev->duration));

                    memcpy(&packet->data[sizeof(rtp_header_t)], &dtmf_packet->data[sizeof(rtp_header_t)], dtmf_packet->len - sizeof(rtp_header_t));
                    get_packet_buffer()->get_packet_pool()->push(dtmf_packet);
                }
            } else if (m_dtmf_marker) {
                m_dtmf_marker = false;
                struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);
                rtp->markerbit = true;
            }
        }
        const evutil_socket_t& remote_socket = packet->rtcp ? get_remote_rtcp_socket() :
                                                              get_remote_rtp_socket();
        const struct sockaddr_in& remote_addr = packet->rtcp ? get_remote_rtcp_addr() :
                                                               get_remote_rtp_addr();
/*
        struct rtp_header_t* rtp_header = reinterpret_cast<struct rtp_header_t*>(&packet->data);
        fprintf(stderr, "endpoint: current packet seqnumber=%d\n", rtp_header->seq_number);
        char buffer[18];
        const char* result=inet_ntop(AF_INET,&remote_addr.sin_addr,buffer,sizeof(buffer));
        fprintf(stderr, "endpoint %s: Send to addr%s:%d\n", id().c_str(),result, htons(remote_addr.sin_port));
*/
        sendto(remote_socket, packet->data, packet->len, 0, (struct sockaddr*) &remote_addr, sizeof(remote_addr));
        // printf("Send RTP %d -> %s:%d len=%d\n", id(), inet_ntoa(remote_addr.sin_addr), ntohs(remote_addr.sin_port), packet->len);
        return false;
    }

    mediaserver::Media local_media() {
    	init_local_media();
    	return m_local_media; 
    }

    mediaserver::Media remote_media() const { return m_remote_media; }
    void update_remote_media(const mediaserver::Media& new_remote_media) { 
    	m_remote_media = new_remote_media; 
    	init_local_media();
    	update_remote_sockets();
    }

    void set_remote_rtp_socket(const evutil_socket_t& socket) { m_remote_rtp_socket = socket; }
    const evutil_socket_t& get_remote_rtp_socket() const { return m_remote_rtp_socket; }
    void set_remote_rtp_addr(struct sockaddr_in& remote_addr) { m_remote_rtp_addr = remote_addr; }
    const struct sockaddr_in& get_remote_rtp_addr() const { return m_remote_rtp_addr; }

    void set_remote_rtcp_socket(const evutil_socket_t& socket) { m_remote_rtcp_socket = socket; }
    const evutil_socket_t& get_remote_rtcp_socket() const { return m_remote_rtcp_socket; }
    void set_remote_rtcp_addr(struct sockaddr_in& remote_addr) { m_remote_rtcp_addr = remote_addr; }
    const struct sockaddr_in& get_remote_rtcp_addr() const { return m_remote_rtcp_addr; }

    bool detect_dtmf(packet_t* packet) {
    	bool first_packet = m_first_packet;
    	m_first_packet = false;
    	struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);

    	// TODO: брать type из Media
    	bool dtmf = rtp->type == 101;
    	if (rtp->markerbit > 0 && !first_packet) {
			// printf("mark packet seq_number=%x timestamp=%d ssrc=%x\n", ntohs(rtp->seq_number), ntohl(rtp->timestamp), ntohl(rtp->ssrc));
		} else if (m_dtmf_pqueue->empty()) {
			return dtmf;
		}

		if (dtmf) {
    		// outband
    		struct dtmf_event_t* dtmf = reinterpret_cast<struct dtmf_event_t*>(&packet->data[sizeof(rtp_header_t)]);
    		// printf("rtp event id=%d e=%d r=%d volume=%d duration=%d\n", dtmf->event, dtmf->e, dtmf->r, dtmf->volume, ntohs(dtmf->duration));

    		packet_pool* ppool = get_packet_buffer()->get_packet_pool();
    		packet_t* dtmf_packet = ppool->pull_zero();
			memcpy(dtmf_packet, packet, sizeof(packet_t));
    		m_dtmf_pqueue->enqueue(dtmf_packet);

    		if (dtmf->e) {
    			while (!m_dtmf_pqueue->empty()) {
    				packet_t* p = NULL;
                	if (!m_dtmf_pqueue->dequeue(&p)) continue;
                	ppool->push(p);
                }

                // printf("outband dtmf endpoint=%s id=%d ssrc=%x\n", id().c_str(), dtmf->event, ntohl(rtp->ssrc)); 
                mediaserver::MediaServerMsg msg;
                msg.mutable_obj()->set_mg(mg());
                msg.mutable_obj()->set_id(id());
                msg.set_dtmf((uint8_t)dtmf->event);
                std::string fe_msg = msg.SerializeAsString();

                if (m_output != NULL) {
                	int res = evbuffer_add(m_output, fe_msg.c_str(), (int)fe_msg.length());
                	if (res == -1) {
                		fprintf(stderr, "evbuffer_add return -1\n");
                	} 
    				// printf("Sending: %d res=%d\n", (int)fe_msg.length(), res);
                } else {
                	fprintf(stderr, "output buffer is NULL\n");
                }
    		}
    		return true;
    	}

    	// TODO:
    	// inband
    	return false;
    	bool end_packet = true;
    	int16_t rtp_data_len = (int16_t)(packet->len - sizeof(rtp_header_t));
    	for (int16_t i = 0; i < rtp_data_len; ++i) {
    		if (packet->data[sizeof(rtp_header_t) + i] != 0xFF) {
    			end_packet = false;
    			break;
    		}
    	}

    	packet_pool* ppool = get_packet_buffer()->get_packet_pool();
    	if (end_packet) {
    		if (m_dtmf_pqueue->empty()) {
    			return false;
    		}
    		printf("Start process dtmf packets <--\n");	

    		/*
    		uint16_t dtmf_data_len = 0;
            uint8_t dtmf_data[800]; // предполагаем, что максимум 5 пакетов по 160 байт
            memset(&dtmf_data, 0, sizeof(dtmf_data));
            */
    		while (!m_dtmf_pqueue->empty()) {
    			packet_t* p = NULL;
                if (!m_dtmf_pqueue->dequeue(&p)) continue;
                /*
                if (dtmf_data_len + rtp_data_len <= 800) {
                	memcpy(&dtmf_data[dtmf_data_len], &p->data, rtp_data_len);
                	dtmf_data_len += rtp_data_len;
                } 
                */     
    			ppool->push(p);
    		}
    		// printf("dtmf_data_len=%d\n", dtmf_data_len);
    		// TODO: тут надо детектить передаваемый DTMF

    		printf("End process dtmf packets -->\n");
    	} else {
    		printf("dtmf packet\n");
			packet_t* dtmf_packet = ppool->pull_zero();
			memcpy(dtmf_packet, packet, sizeof(packet_t));
    		m_dtmf_pqueue->enqueue(dtmf_packet);
    	}
    }

    void send_dtmf(uint8_t dtmf) {
    	std::list<packet_t*> packet_list;
    	packet_list.push_back(create_dtmf_packet(dtmf, 160, true, false));
    	packet_list.push_back(create_dtmf_packet(dtmf, 160 * 2, false, false));
    	packet_list.push_back(create_dtmf_packet(dtmf, 160 * 3, false, false));
    	packet_list.push_back(create_dtmf_packet(dtmf, 160 * 4, false, true));
    	packet_list.push_back(create_dtmf_packet(dtmf, 160 * 5, false, true));

    	for(auto& p : packet_list) {
    		m_dtmf_out_pqueue->enqueue(p);
    	}
    	m_dtmf_marker = true;
    }

private:
	packet_t* create_dtmf_packet(uint8_t dtmf, uint16_t duration, bool marker, bool end) {
		packet_t* packet = get_packet_buffer()->get_packet_pool()->pull_zero();
		if (packet == NULL) {
			fprintf(stderr, "Couldn't get zerro packet");
		}
		struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);
		rtp->type = 101;
		rtp->markerbit = marker;

    	struct dtmf_event_t* dtmf_ev = reinterpret_cast<struct dtmf_event_t*>(&packet->data[sizeof(rtp_header_t)]);
    	dtmf_ev->event = dtmf;
    	dtmf_ev->e = end;
    	dtmf_ev->volume = 10;
    	dtmf_ev->duration = htons(duration);

    	packet->len = sizeof(rtp_header_t) + sizeof(dtmf_event_t);
    	return packet;
	}

	void init_local_media() {
		if (!m_local_media.ip().empty()) {
			return;
		}
		std::string wan_ip = net::get_eth_ipv4();
		uint16_t port = 0;
		do {
			port = get_next_port();
		} while (!bind_rtp_rtcp(port));
		m_local_media.set_media(mediaserver::Media_MediaType::Media_MediaType_Audio);
		std::string ip_port = wan_ip;
		ip_port.append(":");
		ip_port.append(std::to_string(port));
		m_local_media.set_ip(ip_port);
		m_local_media.set_protocol(mediaserver::Media_MediaProtocol::Media_MediaProtocol_RTP_AVP);
		m_local_media.set_direction(mediaserver::Media_MediaDirection::Media_MediaDirection_Sendrecv);
		
		init_codecs(m_local_media);
	}

	void init_codecs(mediaserver::Media& media) {
		mediaserver::Codec* pcmu_codec = media.add_codeclist();
		pcmu_codec->set_id("0");
		pcmu_codec->set_name(CODEC_NAME_PCMU);

		mediaserver::Codec* pcma_codec = media.add_codeclist();
		pcma_codec->set_id("8");
		pcma_codec->set_name(CODEC_NAME_PCMA);

		mediaserver::Codec* g729_codec = media.add_codeclist();
		g729_codec->set_id("18");
		g729_codec->set_name(CODEC_NAME_G729);
		
		mediaserver::Codec* dtmf_codec = media.add_codeclist();
		dtmf_codec->set_id("101");
		dtmf_codec->set_name(CODEC_NAME_TELEPHONE_EVENT);
		// dtmf_codec->set_format("0-16,32-49,64-89");
		dtmf_codec->set_format("0-16");
	}

	void update_remote_sockets() {
		if (m_remote_media.ip().empty()) {
			return;
		}
		m_remote_rtp_socket = m_rtp_udpsock_fd;
		m_remote_rtcp_socket = m_rtcp_udpsock_fd;

		std::string ip = m_remote_media.ip();
		// fprintf(stderr, "remote_media_ip=%s, endpointId=%d\n", ip.c_str(), id());
		// printf("remote_media_ip=%s\n", ip.c_str());
		size_t index = ip.find(":");
		std::string addr = ip.substr(0, index);
		std::string port = ip.substr(index + 1, ip.length() - index - 1);
		uint16_t port_int = std::stoi(port);
		// printf("ADDR=%s PORT=%s\n", addr.c_str(), port.c_str());

		memset(&m_remote_rtp_addr, 0, sizeof(struct sockaddr_in));
 		m_remote_rtp_addr.sin_addr.s_addr = inet_addr(addr.c_str());
		m_remote_rtp_addr.sin_port = htons(port_int);
		m_remote_rtp_addr.sin_family = AF_INET;

		memset(&m_remote_rtcp_addr, 0, sizeof(struct sockaddr_in));
 		m_remote_rtcp_addr.sin_addr.s_addr = inet_addr(addr.c_str());
		m_remote_rtcp_addr.sin_port = htons(port_int + 1);
		m_remote_rtcp_addr.sin_family = AF_INET;
	}

	bool bind_socket_event(const uint16_t& port, int& udpsock_fd, struct sockaddr_in& addr, struct event& evt, event_callback_fn evt_fn) {
		if ((udpsock_fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1) {
			LOG_DEBUG("Error: errno=%d  - unable to create socket", errno);
			return false;
		}

		int nReqFlags = fcntl(udpsock_fd, F_GETFL, 0);
		if (nReqFlags < 0) {
			LOG_DEBUG_STR("Error: cannot get socket options");
			close(udpsock_fd);
			return false;
		}

		if (fcntl(udpsock_fd, F_SETFL, nReqFlags | O_NONBLOCK) < 0) {
			LOG_DEBUG_STR("Error: cannot set socket options");
			close(udpsock_fd);
			return false;
		}

		memset(&addr, 0, sizeof(struct sockaddr_in));
		// stAddr[i].sin_addr.s_addr = inet_addr("10.0.0.108");
		addr.sin_addr.s_addr = INADDR_ANY;
		addr.sin_port = htons(port);
		addr.sin_family = AF_INET;

		int nOptVal = 1;
		if (setsockopt(udpsock_fd, SOL_SOCKET, SO_REUSEADDR, (const void *)&nOptVal, sizeof(nOptVal))) {
			LOG_DEBUG_STR("Error: Error at setsockopt");
			close(udpsock_fd);
			return false;
		}

		int err = bind(udpsock_fd, (struct sockaddr*)&addr, sizeof(addr)); 
		if (err != 0) {
			LOG_DEBUG("Error: Unable to bind port=%d err=%d", (int)port, err);
			close(udpsock_fd);
			return false;
		}

		event_assign(&evt, get_packet_buffer()->get_event_base(), (int)udpsock_fd, EV_READ | EV_PERSIST, evt_fn, this);
		if (event_add(&evt, NULL) == -1) {
			LOG_DEBUG_STR("Error: Can't add event");
			close(udpsock_fd);
			return false;
		}

		return true;
	}

	bool bind_rtp(const uint16_t& port) {
		LOG_DEBUG("bind rtp=%d", port);
		return bind_socket_event(port, m_rtp_udpsock_fd, m_rtp_addr, m_rtp_event, (event_callback_fn)&endpoint::rtp_packet_event);
	}

	bool bind_rtcp(const uint16_t& port) {
		LOG_DEBUG("bind rtcp=%d", port);
		return bind_socket_event(port, m_rtcp_udpsock_fd, m_rtcp_addr, m_rtcp_event, (event_callback_fn)&endpoint::rtcp_packet_event);
	}

	bool bind_rtp_rtcp(const uint16_t& port) {
		bool res = bind_rtp(port);
		if (!res) {
			return res;
		}
		return bind_rtcp(port + 1);
	}

	void init_dtmf_pqueue() {
		mzones_list queue_mzones;
        rte_memzone_t queue_mzone;
        queue_mzone.length = 1024 * sizeof(uintptr_t);
        queue_mzone.addr = (uintptr_t)malloc(queue_mzone.length);
        queue_mzones.push_back(queue_mzone);
        m_dtmf_pqueue = new packet_queue(queue_mzones);

        mzones_list out_queue_mzones;
        rte_memzone_t out_queue_mzone;
        out_queue_mzone.length = 1024 * sizeof(uintptr_t);
        out_queue_mzone.addr = (uintptr_t)malloc(out_queue_mzone.length);
        out_queue_mzones.push_back(out_queue_mzone);
        m_dtmf_out_pqueue = new packet_queue(out_queue_mzones);
	}

	void delete_dtmf_pqueue() {
		if (m_dtmf_pqueue != NULL) {
			delete m_dtmf_pqueue;
			m_dtmf_pqueue = NULL;
		}
		if (m_dtmf_out_pqueue != NULL) {
			delete m_dtmf_out_pqueue;
			m_dtmf_out_pqueue = NULL;
		}
	}

	struct evbuffer* get_output() { return m_output; }

private:
    mediaserver::Media m_local_media;   // media, которое сервер отдает эндпоинту
    mediaserver::Media m_remote_media;	// media, пришедшее от эндпоинта

    // local
    struct event m_rtp_event;
	int m_rtp_udpsock_fd;
	struct sockaddr_in m_rtp_addr;

	struct event m_rtcp_event;
	int m_rtcp_udpsock_fd;
	struct sockaddr_in m_rtcp_addr;

	// remote
	evutil_socket_t m_remote_rtp_socket; 
	struct sockaddr_in m_remote_rtp_addr;
	evutil_socket_t m_remote_rtcp_socket; 
	struct sockaddr_in m_remote_rtcp_addr;

	// dtmf
	packet_queue* m_dtmf_pqueue;
	bool m_first_packet;
	struct evbuffer *m_output;
	packet_queue* m_dtmf_out_pqueue;
	bool m_dtmf_marker;
};

#endif // _ENDPOINT_H
