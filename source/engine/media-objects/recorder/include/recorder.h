#ifndef _RECORDER_H
#define _RECORDER_H

#include <media-obj.h>
#include <packet-buffer.h>

#include <wav.h>

struct recorder : media_object_t {
public:
	static const uint64_t BLOCK_SIZE = 1024 * 1024 * 1;
public:
	recorder(const uint32_t& mg, packet_buffer* buffer, const std::string& file_path) : 
		m_lock(0),
		media_object_t(mg, buffer),
		m_file_path(file_path),
		m_fd(NULL) {
		m_file_data_len = BLOCK_SIZE;
		m_file_data = (uint8_t*)malloc(m_file_data_len);
		m_curr_pos = sizeof(header_wave_t) + sizeof(data_wave_t);
		header_wave_t* header = reinterpret_cast<header_wave_t*>(m_file_data);
		data_wave_t* data = reinterpret_cast<data_wave_t*>(&m_file_data[sizeof(header_wave_t)]);
		header->chunk_id[0] = 'R'; header->chunk_id[1] = 'I'; header->chunk_id[2] = 'F'; header->chunk_id[3] = 'F';
		header->format[0] = 'W'; header->format[1] = 'A'; header->format[2] = 'V'; header->format[3] = 'E';
		header->fmt_chunk_id[0] = 'f'; header->fmt_chunk_id[1] = 'm'; header->fmt_chunk_id[2] = 't'; header->fmt_chunk_id[3] = ' ';
		header->fmt_chunk_size = 18;
		header->audio_format = 1;
		header->num_channels = 1;
		header->sample_rate = 8000;
		header->bits_per_sample = 16;
		header->block_align = header->num_channels * header->bits_per_sample / 8;
		header->byte_rate = header->sample_rate * header->num_channels * header->bits_per_sample / 8;

		data->chunk_id[0] = 'd'; data->chunk_id[1] = 'a'; data->chunk_id[2] = 't'; data->chunk_id[3] = 'a';
	}

	virtual ~recorder() {
		if (m_fd == NULL) {
			return;
		}
		header_wave_t* header = reinterpret_cast<header_wave_t*>(m_file_data);
		data_wave_t* data = reinterpret_cast<data_wave_t*>(&m_file_data[sizeof(header_wave_t)]);
		header->chunk_size = m_curr_pos - 8;
		header->fmt_chunk_size = 16;
		data->chunk_size = m_curr_pos - sizeof(header_wave_t) - sizeof(data_wave_t);

		size_t res = fwrite(m_file_data, 1, m_curr_pos, m_fd);
		if (res != m_curr_pos) {
			printf("Can't write all data in the file %s\n", m_file_path.c_str());
		}

		if (m_fd != NULL) {
			fclose(m_fd);
		}
		free(m_file_data);
	}

	bool init_recorder() {
		bool create = create_path();
		if (!create) return create;
        return create_file();
    }

	virtual bool process_packet(packet_t* packet) {
		if (packet->rtcp) {
			return false;
		}

		uint64_t len = packet->len - sizeof(rtp_header_t);
		lock();
		if (m_curr_pos + len > m_file_data_len) {
			m_file_data = (uint8_t*)realloc(m_file_data, m_file_data_len + BLOCK_SIZE);
			__sync_fetch_and_add(&m_file_data_len, BLOCK_SIZE);
			printf("m_file_data_len=%d\n", (int)m_file_data_len);
		}
		unlock();
		uint64_t pos = __sync_fetch_and_add(&m_curr_pos, len);
		memcpy((void*)&m_file_data[pos], &packet->data[sizeof(rtp_header_t)], len);

		// struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&packet->data);
		// printf("seq_number=%x timestamp=%d ssrc=%x\n", ntohs(rtp->seq_number), ntohl(rtp->timestamp), ntohl(rtp->ssrc));

		return false;
	}

	const char* file() {
		return m_file_path.c_str();
	}

private:
	static int do_mkdir(const char* path, mode_t mode) {
	    struct stat st;
	    int status = 0;
	    if (stat(path, &st) != 0) {
	        if (mkdir(path, mode) != 0 && errno != EEXIST) {
	            status = -1;
	        }
	    } else if (!S_ISDIR(st.st_mode)) {
	        errno = ENOTDIR;
	        status = -1;
	    }
	    return status;
	}

	static int mkpath(const char* file, mode_t mode) {
		std::string tmp(file);
		int pos = 1;
		int status = 0;
		do {
			pos = tmp.find('/', pos);
			if (pos == -1) {
				break;
			}
			pos++;
			std::string path = tmp.substr(0, pos);
			status = do_mkdir(path.c_str(), mode);
		} while (status == 0);
	    return status;
	}

	bool create_path() {
		int rc = mkpath(file(), 0777);
        if (rc != 0) {
            fprintf(stderr, "failed to create (%d: %s): %s\n", errno, strerror(errno), file());
            return false;
        }
        return true;
	}
	bool create_file() {
		if (!(m_fd = fopen(file(), "wb"))) {
			LOG_DEBUG("Can't create file %s", file());
            return false;
		}
        return true;
	}

	inline void lock() {
        while(__sync_lock_test_and_set(&m_lock, 1)) {
            while(m_lock) asm volatile ("rep; nop" ::: "memory");
        }
    }
    inline void unlock() {
        __sync_lock_release(&m_lock);
    }

private:
	volatile int m_lock;
	const std::string m_file_path;
	FILE* m_fd;

	uint8_t* m_file_data;
	uint64_t m_file_data_len;
	uint64_t m_curr_pos;
};

#endif // _RECORDER_H 