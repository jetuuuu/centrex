#ifndef _MEDIA_OBJECT_TABLE_H
#define _MEDIA_OBJECT_TABLE_H

#include "media-obj.h"

struct media_object_table {
public:
	static const uint16_t TABLE_SIZE = 65535;
	union table_node {
        volatile uint128_t value;
        struct {
            volatile uintptr_t ptr;
            struct {
				volatile uint64_t tag:48;
				volatile uint64_t del:1;
				volatile uint64_t busy:15;
			};
        };
    };

	media_object_table(const media_object_table&) = delete;
    media_object_table& operator=(const media_object_table&) = delete;

    media_object_table() {
    	memset(&m_tbl, 0, sizeof(m_tbl));
    }

    ~media_object_table() {
    }

    uintptr_t get_object(const uint16_t& oid) {
    	return m_tbl[oid].ptr;
    }

    uint16_t add_object(media_object_t* obj) {
    	table_node new_node;
    	memset(&new_node, 0, sizeof(table_node));
    	new_node.ptr = reinterpret_cast<uintptr_t>(obj);
    	uint16_t oid = 1;
    	for (; oid < TABLE_SIZE; ++oid) {
    		if (m_tbl[oid].ptr == 0) {
    			table_node curr_node = m_tbl[oid];
    			barrier();
    			new_node.tag = curr_node.tag + 1;
    			if (__sync_bool_compare_and_swap(&m_tbl[oid].value, curr_node.value, new_node.value)) {
    				break;
    			}
    		}
    	}
    	if (oid == TABLE_SIZE) {
    		return 0;
    	}
    	return oid;
    }

    bool is_mark_del(const uint16_t& oid) {
    	return m_tbl[oid].del;
    }

    void mark_object_del(const uint16_t& oid) {
    	m_tbl[oid].del = true;
    }

    void remove_object(const uint16_t& oid) {
		// LOG_DEBUG("delete oid=%d", oid);
    	if (!m_tbl[oid].del) {
    		mark_object_del(oid);
    	}
    	table_node new_node;
    	memset(&new_node, 0, sizeof(table_node));
    	new_node.tag = m_tbl[oid].tag + 1;
    	while (m_tbl[oid].busy != 0) {
    		// если в моменет удаления объекта из таблицы, какой-то из процессоров его еще использует для обработки пакета
    		// printf("oops! wait... oid=%d\n", oid);
    		sched_yield();	
    	}
    	m_tbl[oid] = new_node;
    	// LOG_DEBUG("delete oid=%d complete!", oid);
    }

    void set_busy(const uint16_t& oid, const uint8_t& core) {
    	uint16_t flag = 1 << core;
    	m_tbl[oid].busy |= flag;
	}

	void reset_busy(const uint16_t& oid, const uint8_t& core) {
		uint16_t flag = 1 << core;
    	m_tbl[oid].busy ^= flag;
	}

private:
	table_node m_tbl[TABLE_SIZE];
};

#endif // _MEDIA_OBJECT_TABLE_H