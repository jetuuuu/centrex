#ifndef _MEDIA_OBJECTS_H
#define _MEDIA_OBJECTS_H

#include <cms.h>
#include <cms-logger.h>

#include <endpoint.h>
#include <player.h>
#include <recorder.h>
#include <transcoder.h>
#include <mixer.h>
#include <simple-mixer.h>
#include <empty-object.h>
#include <srtp.h>

#include "media-obj.h"
#include "media-object-table.h"

struct media_objects {
public:
	media_objects(const uint32_t& mg, packet_buffer* buffer) : m_mg(mg), m_buffer(buffer), m_lock(0) {}
	virtual ~media_objects() {}

	uint32_t create_endpoint(const mediaserver::Media& remote_media, struct evbuffer* output) {	
		endpoint* obj = new endpoint(m_mg, m_buffer, remote_media, output);
		if (!obj) {
			LOG_DEBUG_STR("can't create endpoint");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_video_mixer() {
		mixer* obj = new mixer(m_mg, m_buffer);
		if (!obj) {
			LOG_DEBUG_STR("can't create video mixer");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_player(const std::string& file_path) {	
		player* obj = new player(m_mg, m_buffer, file_path);
		if (!obj) {
			LOG_DEBUG_STR("can't create player");
			return 0;
		}
		if (!obj->init_palayer()) {
			delete obj;
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_recorder(const std::string& file_path) {	
		recorder* obj = new recorder(m_mg, m_buffer, file_path);
		if (!obj) {
			LOG_DEBUG_STR("can't create recorder");
			return 0;
		}
		if (!obj->init_recorder()) {
			delete obj;
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_transcoder(const mediaserver::Codec& in_codec, const mediaserver::Codec& out_codec) {
		transcoder* obj = new transcoder(m_mg, m_buffer, in_codec, out_codec);
		if (!obj) {
			LOG_DEBUG_STR("can't create transcoder");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_srtp(const std::string& crypto, const bool& decrypt) {
		srtp* obj = new srtp(m_mg, m_buffer, crypto, decrypt);
		if (!obj) {
			LOG_DEBUG_STR("can't create srtp");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_mixer() {
		mixer* obj = new mixer(m_mg, m_buffer);
		if (!obj) {
			LOG_DEBUG_STR("can't create mixer");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_simple_mixer() {
		simple_mixer* obj = new simple_mixer(m_mg, m_buffer);
		if (!obj) {
			LOG_DEBUG_STR("can't create simple_mixer");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	uint32_t create_empty_object() {
		empty_object* obj = new empty_object(m_mg, m_buffer);
		if (!obj) {
			LOG_DEBUG_STR("can't create empty_object");
			return 0;
		}

		add_object(reinterpret_cast<media_object_t*>(obj));
		return obj->id();
	}

	void destroy_object(const uint32_t& id) {
		media_object_t* obj = get_object(id);
		if (obj == NULL) {
			LOG_DEBUG("media object with id=\"%d\" not found", id);
			return;
		}
		remove_object(id, obj->tbl_oid());
		delete obj;
	}

	media_object_t* get_object_from_table(const uint16_t& tbl_oid) {
		return reinterpret_cast<media_object_t*>(m_tbl.get_object(tbl_oid));
	}

	void set_busy(const uint16_t& tbl_oid, const uint8_t& core) {
		m_tbl.set_busy(tbl_oid, core);
	}

	void reset_busy(const uint16_t& tbl_oid, const uint8_t& core) {
		m_tbl.reset_busy(tbl_oid, core);
	}

	bool is_mark_del(const uint16_t& oid) {
    	return m_tbl.is_mark_del(oid);
    }

	media_object_t* get_object(const uint32_t& id) {
		media_object_t* obj = NULL;
		lock();
		for (const auto& i : m_objects) {
			if (i.first == id) {
				obj = i.second;
				break;
			}
		}
		unlock();
		return obj;
	}

	uint32_t const size() {
		lock();
		uint32_t size = m_objects.size();
		unlock();
		return size;
	}

private:
	void add_object(media_object_t* obj) {
		lock();		
		m_objects.insert(std::make_pair(obj->id(), obj));
		obj->set_tbl_oid(m_tbl.add_object(obj));
		if (obj->tbl_oid() == 0) {
			fprintf(stderr, "can't add object\n");
		}
		unlock();
		LOG_DEBUG("add object id=%d tbl_oid=%d", obj->id(), obj->tbl_oid());
	}

	void remove_object(const uint32_t& id, const uint16_t& tbl_oid) {
		lock();
		m_tbl.mark_object_del(tbl_oid);
    	m_tbl.remove_object(tbl_oid);
		for (auto it = m_objects.cbegin(); it != m_objects.cend();) {
			if (it->first == id) {
    			m_objects.erase(it++);
    			break;
    		} else {
    			++it;
    		}
    	}
    	unlock();
    	LOG_DEBUG("remove object id=%d tbl_oid=%d", id, tbl_oid);
	}

	inline void lock() {
        while(__sync_lock_test_and_set(&m_lock, 1)) {
            while(m_lock) asm volatile ("rep; nop" ::: "memory");
        }
    }
    inline void unlock() {
        __sync_lock_release(&m_lock);
    }

private:
	volatile int m_lock;
	const uint32_t m_mg;
	packet_buffer* const m_buffer;
	std::map<uint32_t, media_object_t*> m_objects;

	media_object_table m_tbl;
};

#endif // _MEDIA_OBJECTS_H