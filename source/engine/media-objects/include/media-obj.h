#ifndef _MEDIA_OBJ_H
#define _MEDIA_OBJ_H

#include <cms.h>
#include <cms-net.h>
#include <packet-buffer.h>

#include <sstream>
static uint32_t get_next_id() {
	static volatile uint32_t s_id = 1;
	return __sync_fetch_and_add(&s_id, 1);
}

struct media_object_t {
public:
	media_object_t(const uint32_t& mg, packet_buffer* buffer) : 
		m_buffer(buffer),
		m_recv(NULL) {
        m_obj.mg = mg;
        m_obj.id = get_next_id();
        m_obj.tbl_oid = 0;
    }

    virtual ~media_object_t() {
    	if (m_recv != NULL) {
    		delete m_recv;
    	}
    }

    uint32_t const mg() const { return m_obj.mg; }
    uint32_t const id() const { return m_obj.id; }
    uint16_t const tbl_oid() const { return m_obj.tbl_oid; }
    void set_tbl_oid(const uint16_t& tbl_oid) { m_obj.tbl_oid = tbl_oid; }
    struct packet_buffer* get_packet_buffer() { return m_buffer; }

    receiver_t* receiver() { return m_recv; }
    virtual void set_receiver(receiver_t* recv) { 
    	if (m_recv != NULL && recv == NULL) {
            LOG_DEBUG("media_object=%d reset receiver %d:%d", id(), m_recv->obj.mg, m_recv->obj.id);
    		receiver_t* tmp = m_recv;
    		m_recv = NULL;
    		delete tmp;
    		return;
    	}
    	m_recv = recv; 
    	LOG_DEBUG("media_object=%d set_receiver %d:%d", id(), m_recv->obj.mg, m_recv->obj.id);
    }

    virtual bool process_packet(packet_t* packet) = 0;

private:
    obj_t m_obj;
	struct packet_buffer* m_buffer;
    receiver_t* m_recv;
};

#endif // #define _MEDIA_OBJ_H
