#ifndef _CRYPTO_SRTP_H
#define _CRYPTO_SRTP_H

#include <media-obj.h>
#include <packet-buffer.h>

struct srtp : media_object_t {
public:
	srtp(const uint32_t& mg, packet_buffer* buffer, const std::string& crypto, bool decrypt) : 
		media_object_t(mg, buffer),
		m_crypto(crypto),
		m_decrypt(decrypt) {
	}
	virtual ~srtp() {
	}

	virtual bool process_packet(packet_t* packet) {
		packet_pool* ppool = get_packet_buffer()->get_packet_pool();
		packet_t* out_packet = ppool->pull_zero();

		if (m_decrypt) {
			// srtp_decrypt
		} else {
			// srtp_encrypt
		}

		return true;
	}

private:
	std::string m_crypto;
	bool m_decrypt;
};

#endif // _CRYPTO_SRTP_H 