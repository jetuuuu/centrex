#ifndef _WAV_H
#define _WAV_H

// https://ccrma.stanford.edu/courses/422/projects/WaveFormat/
struct header_wave_t {
	// RIFF
	char 	chunk_id[4];		// "RIFF"
	int32_t chunk_size;
	char	format[4];			// "WAVE"

	// "fmt" sub-chunk
	char 	fmt_chunk_id[4];	// "fmt "
	int32_t	fmt_chunk_size;		// 18 (16)
								// This is the size of the rest of the Subchunk which follows this number.
	int16_t	audio_format;		// PCM = 1 (i.e. Linear quantization) 
								// Values other than 1 indicate some  form of compression.
	int16_t	num_channels;		// Mono = 1, Stereo = 2, etc.
	int32_t sample_rate;		// 8000, 44100, etc.
	int32_t byte_rate;			// == sample_rate * num_channels * bits_per_sample / 8
	int16_t block_align;		// == num_channels * bits_per_sample / 8
                               	// The number of bytes for one sample including all channels.
	int16_t bits_per_sample;	// 8 bits = 8, 16 bits = 16, etc.
};

struct fact_wave_t {
	char 	chunk_id[4];		// "fact"
	int32_t	chunk_size;			// 4
	int32_t samle_lenght;		// 
};

struct data_wave_t {
	char 	chunk_id[4];		// "data"
	int32_t	chunk_size;			// == num_samples * num_channels * bits_per_sample/8
                               	// This is the number of bytes in the data.
								// You can also think of this as the size of the read of the subchunk following this number.
};


#endif // _WAV_H