#ifndef _PLAYER_H
#define _PLAYER_H

#include <cms.h>
#include <cms-logger.h>
#include <cms-rtp.h>
#include <cms-rnd.h>
#include <cms-time.h>
#include <cms-net.h>

#include <sys/stat.h>
#include <inttypes.h>

#include <iostream>
#include <string>
#include <fstream>
using namespace std; 

#include "wav.h"
#include "packet-buffer.h"
#include <media-obj.h>

struct player : media_object_t {
public:
	player(const uint32_t& mg, packet_buffer* buffer, const std::string& file_path) : 
		m_data(NULL),
		m_header(NULL),
        media_object_t(mg, buffer),
		m_file_path(file_path), 
		m_file_data(NULL), 
		m_file_data_len(0), 
		m_curr_pos(0),
		m_start_data(0),
    	m_samples_per_packet(160) {
    }

    virtual ~player() {
    	free_data();
    }

    bool init_palayer() {
        bool b_init = init_file_data();
        if (!b_init) {
            return false;
        }
        init_rtp_heder();
        return true;
    }

    virtual bool process_packet(packet_t* packet) {
        if (!packet->promise) {
            return false;
        }

        if (packet->t.tv_sec > 0) {
            struct timeval cur_time;
            gettimeofday(&cur_time, NULL);
            if (compare_timeval(&cur_time, &packet->t) < 0) {
                // еще рано отсылать пакет
                usleep(100);
                return true;
            }
        }

        packet_t* player_packet = get_packet_buffer()->get_packet_pool()->pull_zero();
        if (player_packet) {
            uint16_t nByte = get_next_packet_data(player_packet);
            player_packet->len = nByte;

            receiver_t* recv = receiver();
            if (recv != NULL) {
                memcpy(&player_packet->recv, (void*)recv, sizeof(receiver_t));
            }
            player_packet->stream.obj.id = id();

            get_packet_buffer()->get_packet_queue()->enqueue(player_packet);
            packet->t = get_next_time();
        }

        return true;
    }

    uint16_t get_next_packet_data(packet_t* packet) {
    	uint16_t data_packet_len = (m_samples_per_packet * m_header->num_channels * m_header->bits_per_sample) / 8;
    	if (packet == NULL || (sizeof(packet->data) < data_packet_len)) {
    		return 0;
    	}

    	memcpy(&packet->data, &m_rtp_header, sizeof(rtp_header_t));
    	memcpy(&packet->data[sizeof(rtp_header_t)], m_file_data + m_start_data + m_curr_pos, data_packet_len);

    	m_curr_pos += data_packet_len;
    	if ((m_curr_pos + data_packet_len + sizeof(m_data)) > m_data->chunk_size) {
    		m_curr_pos = 0;
    	}

    	uint16_t seq_number = ntohs(m_rtp_header.seq_number);
    	m_rtp_header.seq_number = htons(seq_number + 1);
    	m_rtp_header.timestamp = htonl(ntohl(m_rtp_header.timestamp) + m_samples_per_packet);
    	if (seq_number == 0) {
    		gettimeofday(&m_start_time, NULL);
    	}

    	return sizeof(m_rtp_header) + data_packet_len;
    }

    virtual void set_receiver(receiver_t* recv) { 
    	media_object_t::set_receiver(recv);
    	push_promise();
    }

    struct timeval get_next_time() {
    	struct timeval t = m_start_time;
    	add_ms_to_timeval(&t, 20 * ntohs(m_rtp_header.seq_number)); // 20ms
    	return t;
    }

private:
	const char* file() const {
    	return m_file_path.c_str();
    }

    bool init_file_data() {
    	bool b_read = read_file();
        if (!b_read) {
            return false;
        }

    	if (m_file_data == NULL) {
    		return false;
    	}

    	m_header = (header_wave_t*)m_file_data;
    	m_data = (data_wave_t*)(m_file_data + 20 + m_header->fmt_chunk_size);
    	m_start_data = 20 + m_header->fmt_chunk_size + sizeof(m_data);
    	if (m_data->chunk_id[0] == 'f' && m_data->chunk_id[1] == 'a' && m_data->chunk_id[2] == 'c' && m_data->chunk_id[3] == 't') {
    		m_data = (data_wave_t*)(m_file_data + 20 + m_header->fmt_chunk_size + sizeof(fact_wave_t));
    		m_start_data += sizeof(fact_wave_t);
    	}
    	m_curr_pos = 0; 
    	memset(&m_start_time, 0, sizeof(m_start_time));

    	print_file_info();
        return true;
    }

    void init_rtp_heder() {
    	memset(&m_rtp_header, 0, sizeof(rtp_header_t));

    	m_rtp_header.version = 2;

    	// 0 by default
    	// m_rtp_header.padding = 0;
    	// m_rtp_header.extension = 0;
    	// m_rtp_header.csrccount = 0;
    	// m_rtp_header.markerbit = 0;
    	// m_rtp_header.type = 8;

    	// m_rtp_header.seq_number = 0; // инкремент на 1
    	// m_rtp_header.timestamp = 0; // инкрмент на 160

    	m_rtp_header.ssrc = htonl(random_uint32(0));
    }

	bool read_file() {
		FILE* fd = NULL;
		struct stat st;
		if (stat(file(), &st) == 0) {
			if ((fd = fopen(file(), "rb"))) {
				m_file_data_len = st.st_size;
				m_file_data = (uint8_t*)malloc(m_file_data_len);
				if (fread(m_file_data, 1, m_file_data_len, fd) < m_file_data_len) {
					fprintf(stderr, "Can't read so match bytes\n");
					LOG_DEBUG("Can't read so match bytes %s", file());
					free_data();
                    return false;
				}
				fclose(fd);
			} else {
				LOG_DEBUG("Can't open file %s", file());
                return false;
			}
		} else {
			LOG_DEBUG("Can't stat file %s", file());
            return false;
		}
        return true;
	}

	void free_data() {
		if (m_file_data == NULL) {
			return;
		}

		m_file_data_len = 0;
		free(m_file_data);
		m_file_data = NULL;
	}

	void push_promise() {
		packet_pool* ppool = get_packet_buffer()->get_packet_pool();
		packet_t* packet = ppool->pull_zero();
		
		packet->promise = true;
        packet->recv.obj.mg = mg();
		packet->recv.obj.id = id();
        packet->recv.obj.tbl_oid = tbl_oid();

		packet_queue* pqueue = get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(packet);
	}

	void print_file_info() {
		if (m_header == NULL) {
			return;
		}

		cout << "chunk_id=" << m_header->chunk_id[0] << m_header->chunk_id[1] << m_header->chunk_id[2] << m_header->chunk_id[3] << endl;
		cout << "chunk_size=" << m_header->chunk_size << endl;
		cout << "format=" << m_header->format[0] << m_header->format[1] << m_header->format[2] << m_header->format[3] << endl;

		cout << "fmt_chunk_id=" << m_header->fmt_chunk_id[0] << m_header->fmt_chunk_id[1] << m_header->fmt_chunk_id[2] << m_header->fmt_chunk_id[3] << endl;
		cout << "fmt_chunk_size=" << m_header->fmt_chunk_size << endl;
		cout << "audio_format=" << m_header->audio_format << endl;
		cout << "num_channels=" << m_header->num_channels << endl;
		cout << "sample_rate=" << m_header->sample_rate << endl;
		cout << "byte_rate=" << m_header->byte_rate << endl;
		cout << "block_align=" << m_header->block_align << endl;
		cout << "bits_per_sample=" << m_header->bits_per_sample << endl;

		cout << "data:" << endl;
		cout << "chunk_id=" << m_data->chunk_id[0] << m_data->chunk_id[1] << m_data->chunk_id[2] << m_data->chunk_id[3] << endl;
		cout << "chunk_size=" << m_data->chunk_size << endl;
	}

private:
	const std::string m_file_path;
	uint8_t* m_file_data;
	uint64_t m_file_data_len;
	header_wave_t* m_header;
   	data_wave_t* m_data;

   	uint64_t m_start_data;
	uint16_t m_samples_per_packet;
	uint64_t m_curr_pos;
	struct timeval m_start_time;

	rtp_header_t m_rtp_header;
};

#endif // _PLAYER_H
