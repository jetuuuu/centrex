#ifndef _SIMPLE_MIXER_H
#define _SIMPLE_MIXER_H

#include <media-obj.h>
#include <packet-buffer.h>

struct simple_mixer : media_object_t {
public:
	simple_mixer(const uint32_t& mg, packet_buffer* buffer) : media_object_t(mg, buffer), m_lock(0) {
	}

	virtual ~simple_mixer() {
		lock();
		for (auto it = m_receiver_map.cbegin(); it != m_receiver_map.cend(); ++it) {
			delete it->second;
    	}
    	for (auto it = m_queue_map.cbegin(); it != m_queue_map.cend(); ++it) {
			delete it->second;
    	}
    	unlock();
	}

	virtual bool process_packet(packet_t* packet) {
		if (packet->rtcp) {
			return false;
		}
		stream_t stream = packet->stream;
		uint64_t s = stream.obj.mg; s <<= 32; s |= stream.obj.id;
		receiver_t* recv = get_receiver(stream);
		if (recv != NULL) {
			memcpy(&packet->recv, (void*)recv, sizeof(receiver_t));
		}
		
		packet_pool* ppool = get_packet_buffer()->get_packet_pool();
		packet_t* _packet = ppool->pull_zero();
		if (_packet == NULL) {
			printf("can't get new packet\n");
			return true;
		}
		memcpy(_packet, packet, sizeof(packet_t));

		lock();
		bool ready = true;
		for (auto it = m_queue_map.cbegin(); it != m_queue_map.cend(); ++it) {
			if (it->first != s) {
				if ((it->second)->empty()) {
					ready = false;
					break;
				}
    		}
    	} 

    	if (ready) {
    		uint16_t data_len = _packet->len - sizeof(rtp_header_t);
    		uint16_t samples = data_len / 2;
    		int16_t* data_ptr = reinterpret_cast<int16_t*>(&_packet->data[sizeof(rtp_header_t)]);
    		if (samples > 160) {
    			struct rtp_header_t* rtp = reinterpret_cast<struct rtp_header_t*>(&_packet->data);
				printf("UDP packet len=%d\n", _packet->len);
				printf("RTP Version:%x Padding:%d Extension:%d csrccount=%d markerbit=%d type=%d\n", rtp->version, rtp->padding, rtp->extension, 
			                                                                               rtp->csrccount, rtp->markerbit, rtp->type);
				printf("RTP seq_number=%x timestamp=%d ssrc=%x\n", ntohs(rtp->seq_number), ntohl(rtp->timestamp), ntohl(rtp->ssrc));
				printf("RTP Payload len=%d\n", (int16_t)(packet->len - sizeof(rtp_header_t)));
    		}
    		// printf("data_len= %d samples=%d\n", data_len, samples);
    		for (auto it = m_queue_map.cbegin(); it != m_queue_map.cend(); ++it) {
				if (it->first != s) {
					packet_queue* queue = it->second;
					if (queue == NULL) {
						continue;
					}
					packet_t* stream_packet = NULL;
	                if (!queue->dequeue(&stream_packet)) {
	                    continue;
	                }
	                if (stream_packet == NULL) {
	                	continue;
	                }

	                int16_t* next_data_ptr = reinterpret_cast<int16_t*>(&stream_packet->data[sizeof(rtp_header_t)]);
	                for (uint16_t i = 0; i < samples; i++) {
	                	int32_t mix_data = data_ptr[i] + next_data_ptr[i];
	                	if (mix_data > 32767) { 
	                		mix_data = 32767;
	                	} else if (mix_data < -32768) {
	                		mix_data = -32768;
	                	}
	                	data_ptr[i] = (int16_t)mix_data;
	                }
	                ppool->push(stream_packet);
	    		}
    		}

    		receiver_t* recv = receiver();
        	if (recv != NULL) {
            	memcpy(&_packet->recv, (void*)recv, sizeof(receiver_t));
        	}
        	packet_queue* pqueue = get_packet_buffer()->get_packet_queue();
			pqueue->enqueue(_packet);
    	} else {
    		packet_queue* queue = m_queue_map[s];
    		if (queue != NULL) {
    			queue->enqueue(_packet);
    		} else {
    			printf("queue for stream %d:%d not found\n", packet->stream.obj.mg, packet->stream.obj.id);
    			ppool->push(_packet);
    		}
    	}
		unlock();

 		return true;
	}

	void attach_stream(const stream_t& stream, receiver_t* recv) {
		LOG_DEBUG("simple_mixer=%d attach_stream %d:%d", id(), stream.obj.mg, stream.obj.id);
		uint64_t s = stream.obj.mg; s <<= 32; s |= stream.obj.id;
		receiver_t* old_receiver = get_receiver(stream); 

    	mzones_list queue_mzones;
        rte_memzone_t queue_mzone;
        queue_mzone.length = 64 * sizeof(packet_t);
        queue_mzone.addr = (uintptr_t)malloc(queue_mzone.length);
        queue_mzones.push_back(queue_mzone);
        packet_queue* queue = new packet_queue(queue_mzones);

    	lock();		
		m_receiver_map[s] = recv;
		m_queue_map[s] = queue;
		unlock();

		if (old_receiver != NULL) {
			delete old_receiver;
		}
	}

	void detach_stream(const stream_t& stream) {
		uint64_t s = stream.obj.mg; s <<= 32; s |= stream.obj.id;
		lock();
		for (auto it = m_receiver_map.cbegin(); it != m_receiver_map.cend();) {
			if (it->first == s) {
				delete it->second;
    			m_receiver_map.erase(it++);
    			break;
    		} else {
    			++it;
    		}
    	}
    	unlock();
	}

private:
	inline void lock() {
        while(__sync_lock_test_and_set(&m_lock, 1)) {
            while(m_lock) asm volatile ("rep; nop" ::: "memory");
        }
    }
    inline void unlock() {
        __sync_lock_release(&m_lock);
    }

    receiver_t* get_receiver(const stream_t& stream) {
		receiver_t* recv = NULL;
		uint64_t s = stream.obj.mg; s <<= 32; s |= stream.obj.id;
		lock();
		for (const auto& i : m_receiver_map) {
			if (i.first == s) {
				recv = i.second;
				break;
			}
		}
		unlock();
		return recv;
	}

private:
	volatile int m_lock;
	std::map<uint64_t, receiver_t*> m_receiver_map;
	std::map<uint64_t, packet_queue*> m_queue_map;
};

#endif // _SIMPLE_MIXER_H 