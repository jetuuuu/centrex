/*
 * payload-type.h
 *
 *  Created on: 09 янв. 2015 г.
 *      Author: nikita
 */

#ifndef SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_PAYLOAD_TYPE_H_
#define SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_PAYLOAD_TYPE_H_


enum TYPE {
	NAL_UNIT,
	STAP_A,
	STAP_B,
	MTAP_16,
	MTAP_24,
	FU_A,
	FU_B
};


#endif /* SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_PAYLOAD_TYPE_H_ */
