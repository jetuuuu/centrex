/*
 * FuHeader.h
 *
 *  Created on: 09 янв. 2015 г.
 *      Author: nikita
 */

#ifndef SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_FUHEADER_H_
#define SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_FUHEADER_H_

#include <bitset>
#include "payload-type.h"

enum S {
	START,
	CONTINUE
};

enum E {
	END,
	NON_END
};

enum R {
	ZERO
};

struct FuHeader {
	static uint8_t get_fu_header(S s, E e, R r, TYPE type) {
		std::bitset<8> fu_header;

		switch (s) {
		case START:
			fu_header[7] = 1;
			break;
		case CONTINUE:
			fu_header[7] = 0;
			break;
		}

		switch (e) {
		case END:
			fu_header[6] = 1;
			break;
		case NON_END:
			fu_header[6] = 0;
			break;
		}

		switch (r) {
		case ZERO:
			fu_header[5] = 0;
			break;
		default:
			fu_header[5] = 0;
			break;
		}

		switch (type) {
		case NAL_UNIT:
			std::bitset<5> t(1);
		    for (int i = 0; i < 5; i++) {
		        fu_header[i] = t[i];
		    }
			break;
		case STAP_A:
			std::bitset<5> t(24);
		    for (int i = 0; i < 5; i++) {
		        fu_header[i] = t[i];
		    }
			break;
		case FU_A:
			std::bitset<5> t(28);
		    for (int i = 0; i < 5; i++) {
		        fu_header[i] = t[i];
		    }
			break;
		default:
			//остальные типы не поддерживаются в Non-Interleaved Mode (как я понял это UDP)
			break;
		}

		return static_cast<uint8_t>(fu_header.to_ulong());
	}
};


#endif /* SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_FUHEADER_H_ */
