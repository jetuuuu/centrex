/*
 * SinglNAL.h
 *
 *  Created on: 09 янв. 2015 г.
 *      Author: nikita
 */

//Singl NAL https://tools.ietf.org/html/rfc6184#section-5.3 H.264 кодек

#ifndef SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_SINGLNAL_H_
#define SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_SINGLNAL_H_

#include <bitset>
#include "payload-type.h"

enum F_HEADER {
	NO_ERRORS,
	HAVE_ERRORS
};

enum NRI {
	NON_IDR,
	DATA_A,
	DATA_B,
	DATA_C,
	DEFAULT
};

struct SinglNAL {
public:
	static uint8_t get_nal_unit_header(F_HEADER f, NRI nri, TYPE type) {
		std::bitset<8> nal_header;

		switch (f) {
		case NO_ERRORS:
			nal_header[7] = 0;
			break;
		case HAVE_ERRORS:
			nal_header[7] = 1;
			break;
		}

		switch (nri) {
		case NON_IDR:
			nal_header[6] = 1;
			nal_header[5] = 0;
			break;
		case DATA_A:
			nal_header[6] = 1;
			nal_header[5] = 0;
			break;
		case DATA_B:
			nal_header[6] = 0;
			nal_header[5] = 1;
			break;
		case DATA_C:
			nal_header[6] = 0;
			nal_header[5] = 1;
			break;
		case DEFAULT:
			nal_header[6] = 0;
			nal_header[5] = 0;
			break;
		}

		switch (type) {
		case NAL_UNIT:
			std::bitset<5> t(1);
		    for (int i = 0; i < 5; i++) {
		        nal_header[i] = t[i];
		    }
			break;
		case STAP_A:
			std::bitset<5> t(24);
		    for (int i = 0; i < 5; i++) {
		        nal_header[i] = t[i];
		    }
			break;
		case FU_A:
			std::bitset<5> t(28);
		    for (int i = 0; i < 5; i++) {
		        nal_header[i] = t[i];
		    }
			break;
		default:
			//остальные типы не поддерживаются в Non-Interleaved Mode (как я понял это UDP)
			break;
		}

		return static_cast<uint8_t>(nal_header.to_ullong());
	}
};


#endif /* SOURCE_ENGINE_MEDIA_OBJECTS_MIXER_INCLUDE_SINGLNAL_H_ */
