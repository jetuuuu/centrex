#ifndef _MIXER_H
#define _MIXER_H

#define SIZE_RTP sizeof(rtp_header_t)
#define MAX_SIZE_PACKET 1500
#define MAX_SIZE_CSRC 15

#define MAX_NALU_SIZE 2
#define NALU_HEDER 1
#define STAP_A_HEADER 1
#define FU_INDICATOR 1
#define FU_HEADER 1

#include <media-obj.h>
#include <packet-buffer.h>
#include <map>
#include <vector>
#include <atomic>
#include <algorithm>

#include "SinglNAL.h"
#include "FuHeader.h"

// TODO:
// 1. Нужно рефакторить media_object_t и делать несколько ресиверов? или городить отдельные списки микшируемых медиаобъектов?
// 2. Нужно подумать как это вообще будет работать.

struct mixer : media_object_t {
public:
	mixer(const uint32_t& mg, packet_buffer* buffer) : media_object_t(mg, buffer) {
	}

	virtual ~mixer() {
        for (auto it = m_packets.begin(); it != m_packets.end(); ++it) {
			delete it->second;
		}
	}

	static mixer* get_instance(const uint32_t& mg, packet_buffer* buffer) {
		if (m_instance == NULL) {
			m_instance = new mixer(mg, buffer);
		}
		return m_instance;
	}

	virtual bool process_packet(packet_t* packet) {
        // TODO: что должен возвращать метод ?
		// TODO: добавить обработку формата payload type (см. таблицу)
		// TODO: добавить CSRC - список индентификаторов(SSRC тех, кто принимал участие в микшировании)
		// TODO: свой SSRC добавить для микшера в RTP конечный

		if (packet->rtcp || isAudio(packet)) {
			return false;
		}

        packet_pool* ppool = get_packet_buffer()->get_packet_pool();
        packet_t* _packet = ppool->pull_zero();
        if (_packet == NULL) {
            printf("can't get new packet\n");
            return true;
        }
        memcpy(_packet, packet, sizeof(packet_t));

        uint64_t id = get_id(packet->stream);

		m_spin_lock.lock();

        m_packets[id] = _packet;
        bool ready = false;
        for (auto it = m_packets.begin(); it != m_packets.end(); ++it) {
            if (it->first != id) {
                if(it->second != NULL) {
                    ready = true;
                    break;
                }
            }
        }
        packet_t general_packet;
        std::vector<receiver_t*> receivers;
        std::vector<uint32_t> csrc(MAX_SIZE_CSRC);
        if (ready) {
            uint8_t* video_data_ptr = &general_packet.data[SIZE_RTP];
            general_packet.len = SIZE_RTP;
            auto it = m_packets.begin();
            while (it != m_packets.end()) {
                //if (it->first != id) {
                    if (it->second != NULL) {
                    	auto length = it->second->len - SIZE_RTP;
                    	if (is_new(&general_packet)) {
                    		if ((size_t)general_packet.len + length + STAP_A_HEADER + MAX_NALU_SIZE + NALU_HEDER <= MAX_SIZE_PACKET) {
                                receivers.push_back(m_receivers[it->first]);
                                csrc.push_back(get_ssrc(it->second));

                    			uint8_t stap_a_header = SinglNAL::get_nal_unit_header(NO_ERRORS, DEFAULT, STAP_A); //генерация stap_a_header
                                memcpy(video_data_ptr, &stap_a_header, sizeof(uint8_t));
                                video_data_ptr += STAP_A_HEADER;
                                general_packet.len += STAP_A_HEADER;

                                init_stap_a_frame(&general_packet, it->second, video_data_ptr);
                                //сейчас в пакете такая тсруктура RTP HEADER + STAP-A HEADER + NALU SIZE + NALU HEADER + DATA

                                ++it;
                            }
                    	} else if (is_stap_a(&general_packet)) {
                    		if ((size_t)general_packet.len + MAX_NALU_SIZE + NALU_HEDER + length <= MAX_SIZE_PACKET) {
                    			receivers.push_back(m_receivers[it->first]);
                    			csrc.push_back(get_ssrc(it->second));

                    			init_stap_a_frame(&general_packet, it->second, video_data_ptr);
                                //струткура пакета RTP + STAP-A + NALU_1 SIZE + NALU_1 HEADER + NALU_2 SIZE + NALU_2 HEADER

                    			++it;
                    		} else {
                    			//если новый пакет не "влезает" в наш общий
                        		init_rtp_header(&general_packet, static_cast<uint16_t>(receivers.size()), csrc);
                        		send_packet(&general_packet, receivers);
                        		csrc.clear();
                        		receivers.clear();
                        		memcpy(0, &general_packet.data[0], MAX_SIZE_PACKET * sizeof(uint8_t));
                        		video_data_ptr = &general_packet.data[SIZE_RTP];
                        		//повторить итерацию
                    		}
                    	}
                    }
                //}
            }
        }

        if (general_packet != NULL) {
        	init_rtp_header(&general_packet, reinterpret_cast<uint16_t>(receivers.size()), csrc);
        	send_packet(&general_packet, receivers);
        }

        //send_packet(&general_packet, receivers);
		m_spin_lock.unlock();

		return false;
	}

    void send_packet(packet_t* packet, std::vector<receiver_t*>& receivers) {
        packet_queue* queue = get_packet_buffer()->get_packet_queue();
        for (int i = 0; i < receivers.size(); i++) {
            memcpy(&packet->recv, receivers[i], sizeof(receiver_t));
            queue->enqueue(packet);
        }
    }

	void attach_stream(const stream_t& stream, receiver_t* receiver) {
		uint64_t id = get_id(stream);

        m_spin_lock.lock();

        m_packets[id] = NULL;
        m_receivers[id] = receiver;

        m_spin_lock.unlock();

	}

	void detach_stream(const stream_t& stream) {
		m_spin_lock.lock();

		uint64_t id = get_id(stream);
        auto it = m_packets.find(id);
		delete it->second;
        m_packets.erase(it);
		m_receivers.erase(m_receivers.find(id));

		m_spin_lock.unlock();
	}
private:
	inline uint64_t get_id(stream_t stream) {
		uint64_t id = stream.obj.mg;
		id <<= 32;
		id |= stream.obj.id;

		return id;
	}

	bool isAudio(packet_t* packet) {
		rtp_header_t header;
		memcpy(&header, packet->data, SIZE_RTP);
		bool isAudio = false;
		if (header.type == 0 || header.type == 3 || header.type == 8 || header.type == 12 || header.type == 14) {
			isAudio = true;
		}
                
		return isAudio;
	}

	uint32_t get_ssrc(packet_t* packet) {
		rtp_header_t header;
		memcpy(&header, packet->data, SIZE_RTP);

		return header.ssrc;
	}

	uint16_t next_number() {
		static uint16_t number = 0;

		return number++;
	}

	void init_rtp_header(packet_t* packet, uint16_t csrccount, std::vector<uint32_t>& csrc) {
		rtp_header_t header;
		header.version = 0;
		header.padding = 0;
		header.extension = 0;
		header.markerbit = ???;
		header.type = 26; //JPEG
		header.seq_number = next_number();
		header.csrccount = csrccount;
		header.timestamp = 0; //что тут выставлять ?
		header.ssrc = 1337; //ssrc мкшера
		memcpy(&header.csrc, &(csrc)[0], sizeof(uint32_t) * csrccount);
		memcpy(packet, &header, SIZE_RTP);
	}

	void init_stap_a_frame(packet_t* general_packet, packet_t* packet, uint8_t* data) {
		auto length = packet->len - SIZE_RTP;
		uint16_t nalu_size = length;
		uint8_t nalu_header = SinglNAL::get_nal_unit_header(NO_ERRORS, DEFAULT, NAL_UNIT);
		memcpy(data, &nalu_size, sizeof(uint16_t));
		data += MAX_NALU_SIZE;
		memcpy(data, &nalu_header, sizeof(uint8_t));
		data += NALU_HEDER;
		memcpy(data, &packet->data[SIZE_RTP], length * sizeof(uint8_t));
		data += length;
		general_packet->len += MAX_NALU_SIZE + NALU_HEDER + length;
	}

	bool is_new(packet_t* packet) {
		return packet->len == SIZE_RTP;
	}

	bool is_stap_a(packet_t* packet) {
		std::bitset<8> type(packet->data[SIZE_RTP + 1]);
		std::bitset<5> stap_a(24);
		bool is_stap_a = false;
		for (size_t i = 0, j = 4; i < 5; i++, j--) {
			is_stap_a = (type[j] == stap_a[i]);
		}
		return is_stap_a;
	}

private:
	static mixer* m_instance;
    std::map<uint64_t, packet_t*> m_packets;
	std::map<uint64_t, receiver_t*> m_receivers;
	struct SpinLock {
	public:
		inline void lock() {
	        while(lck.test_and_set(std::memory_order_acquire)) {}
		}

		inline void unlock() {
			lck.clear(std::memory_order_release);
		}
	private:
		std::atomic_flag lck = ATOMIC_FLAG_INIT;
	};
	SpinLock m_spin_lock;
};

#endif // _MIXER_H 
//TODO: один объект микшера на ?!



/* FU-A packet
 * if ((size_t)general_packet.len + length + FU_INDICATOR + FU_HEADER > MAX_SIZE_PACKET) { //на случай, когда пакет очень большой
                    			auto offset = SIZE_RTP;
                    			size_t i = 0;
                    			while (length > 0) {§
                    				uint8_t fu_indicator;
                    				uint8_t fu_header;
                    				general_packet.len += 2;
                    				auto len =  MAX_SIZE_PACKET - general_packet.len;
                    				if (i == 0) {
                    					fu_indicator = SinglNAL::get_nal_unit_header(NO_ERRORS, DEFAULT, FU_A);
                    					fu_header = FuHeader::get_fu_header(START, NON_END, ZERO, NAL_UNIT);
                    				} else if (i != 0 && (length - len <= 0)) {
                    					fu_indicator = SinglNAL::get_nal_unit_header(NO_ERRORS, DEFAULT, FU_A);
                    					fu_header = FuHeader::get_fu_header(CONTINUE, END, ZERO, NAL_UNIT);
                    				}

                    				csrc.push_back(get_ssrc(it->second));
									memcpy(video_data_ptr, &fu_indicator, sizeof(uint8_t));
		                    		video_data_ptr++;
		                    		memcpy(video_data_ptr, &fu_header, sizeof(uint8_t));
		                    		video_data_ptr++;
		                    		if (length - len <= 0) {
		                    			len = length;
		                    		}
		                    		memcpy(video_data_ptr, &it->second->data[offset], len * sizeof(uint8_t));
	                    			// TODO: заполнить ресиверы на отправку
	                    			init_rtp_header(&general_packet, reinterpret_cast<uint16_t>(receivers.size()), csrc);
	                    			send_packet(&general_packet, receivers);
	                    			csrc.clear();
	                    			i++;
	                    			offset += len;
	                    			length -= len;
                    			}

                    		} else
 * */
