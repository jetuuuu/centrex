#ifndef _CODEC_CONSTS_H
#define _CODEC_CONSTS_H

//en.wikipedia.org/wiki/RTP_audio_video_profile
enum payload_type {
	payload_type_pcmu = 0,
	payload_type_pcma = 8,
	payload_type_l16 = 11, //linear PCM 16b 44100Hz mono
	payload_type_g729 = 18,
	payload_type_l16_8000 = 115 //linear PCM 16b 8000Hz mono, dynamic payload number
};

#define DEFAULT_L16_FULL_FRAME_SIZE  320
#define DEFAULT_L16_HALF_FRAME_SIZE  160

static const char* CODEC_NAME_L16_8000 = "L16/8000";
static const char* CODEC_NAME_PCMA = "PCMA/8000";
static const char* CODEC_NAME_PCMU = "PCMU/8000";
static const char* CODEC_NAME_G722 = "G722/8000";
static const char* CODEC_NAME_G729 = "G729/8000";
static const char* CODEC_NAME_OPUS = "OPUS/8000";
static const char* CODEC_NAME_TELEPHONE_EVENT = "telephone-event/8000";

#endif // _CODEC_CONSTS_H