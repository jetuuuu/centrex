/*****************************************************************************
//
// INTEL CORPORATION PROPRIETARY INFORMATION
// This software is supplied under the terms of a license agreement or
// nondisclosure agreement with Intel Corporation and may not be copied
// or disclosed except in accordance with the terms of that agreement.
// Copyright (c) 2005-2011 Intel Corporation. All Rights Reserved.
//
// Intel(R) Integrated Performance Primitives
//
//     USCI - Unified Speech Codec Interface
//
//     USCI base header
//            Basic structures and functions declarations.
//
*****************************************************************************/

#ifndef __USC_BASE_H__
#define __USC_BASE_H__

#include <stdint.h>

#ifndef NULL
#ifdef  __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif

#if defined( USC_W32DLL )
  #if defined( _MSC_VER ) || defined( __ICL ) || defined ( __ECL )
    #define USCFUN  __declspec(dllexport)
  #else
    #define USCFUN  extern
  #endif
#else
  #define USCFUN  extern
#endif
#define LINEAR_PCM        1
#define ALAW_PCM          6
#define MULAW_PCM         7

typedef enum _G729_PARAMETER
{
  G729_TRUNCATE = 1, //
  G729_VAD = 2,
  G729_HPF = 4, //high pass filter
  G729_PF = 8 //post filter
} G729_PARAMETER;

typedef int G729_PARAMETERS;

/* USC error code */
typedef enum {
   /* errors: negative response */
   USC_UnsupportedFrameSize = -11,
   USC_BadArgument          = -10,
   USC_UnsupportedEchoTail  = -9,
   USC_NotInitialized       = -8,
   USC_InvalidHandler       = -7,
   USC_NoOperation          = -6,
   USC_UnsupportedPCMType   = -5,
   USC_UnsupportedBitRate   = -4,
   USC_UnsupportedFrameType = -3,
   USC_UnsupportedVADType   = -2,
   USC_BadDataPointer       = -1,
   USC_NoError              =  0,
   /* warnings: positive response */
   USC_StateNotChanged      =  1
} USC_Status;

/* USC algorithm type */
typedef enum {
   USC_Codec     = 0,        /* IPP */
   USC_EC        = 1,        /*  a  */
   USC_Filter    = 2,        /*  l  */
   USC_TD        = 3,        /*  g  */
   USC_FilterVAD = 4,        /*  os */
   USC_T38       = 5,        /*     */
   USC_MAX_ALG   = 0xffff    /*     */
} USC_AlgType;

/* USC PCM stream type */
typedef struct {
   int  sample_frequency; /* sample rate in Hz */
   int  bitPerSample;     /* bit per sample */
   int  nChannels;        /* file characteristic */
} USC_PCMType;

/* USC memory types */
typedef enum {
   USC_OBJECT       = 0, /* persistent memory */
   USC_BUFFER       = 1, /* scratch memory */
   USC_CONST        = 2, /* memory for tables, constants  */
   USC_MAX_MEM_TYPES     /* Number of memory types */
} USC_MemType;


/* USC memory space types */
typedef enum {
   USC_NORMAL    = 0,   /* normal memory space */
   USC_MEM_CHIP  = 1,   /* high speed memory*/
   USC_MAX_SPACE        /* Number of memory space types */
} USC_MemSpaceType;

/* USC PCM stream */
typedef struct {
	uint8_t        *pBuffer;   /* PCM data buffer pointer */
   int          nbytes;    /* data buffer size in byte */
   USC_PCMType  pcmType;   /* PCM info */
   int          bitrate;   /* in bps */
} USC_PCMStream;

/*USC bad arguments check helpers
   _USC_NO_BADARG_CHECKS to switch bad agrument check off */
#ifdef _USC_NO_BADARG_CHECKS
   #define USC_BADARG( expr, ErrCode )
#else
   #define USC_BADARG( expr, ErrCode )\
                {if (expr) { return ErrCode; }}
#endif

   #define USC_CHECK_PTR( ptr )\
                USC_BADARG( NULL==(ptr), USC_BadDataPointer )

   #define USC_CHECK_HANDLE( ptr )\
                USC_BADARG( NULL==(ptr), USC_InvalidHandler )

#endif /* __USC_BASE_H__ */
