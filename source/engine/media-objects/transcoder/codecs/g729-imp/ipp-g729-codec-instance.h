#ifndef __ipp_g729_codec_instance_h__
#define __ipp_g729_codec_instance_h__

#include <ippdefs.h>
#include "../common-ipp/usc_base.h"

typedef enum {
   USC_ENCODE = 0, /* Encoder */
   USC_DECODE = 1, /* Decoder */
   USC_DUPLEX = 2, /* Both  */
   USC_MAX_DIRECTION_TYPES /* Number of direction types */
} USC_Direction;

typedef struct {
  int bitrate;         /* in bps */
} USC_Rates;

typedef enum  {
    USC_OUT_NO_CONTROL     =0,
    USC_OUT_MONO           =1,
    USC_OUT_STEREO         =2,
    USC_OUT_COMPATIBLE     =3,
    USC_OUT_DELAY          =4,
    USC_MAX_OUTPUT_MODES
} USC_OutputMode;

/* USC codec modes, (may be alternated) */
typedef struct {
   int            bitrate;    /* in bps */
   int            truncate;   /* 0 - no truncate */
   int            vad;        /* 0 - disabled, otherwize vad type (1,2, more if any) */
   int            hpf;        /* high pass filter: 1- on, 0- off */
   int            pf;         /* post filter / AGC for AMRWB+ : 1- on, 0- off */
   USC_OutputMode outMode;    /* codec output mode */
} USC_Modes;

/* USC codec option */
typedef struct {
   USC_Direction    direction;  /* 0 - encode only, 1 - decode only, 2 - both */
   int              law;        /* 0 - pcm, 1 - aLaw, 2 -muLaw */
   int              framesize;  /* a codec frame size */
   USC_PCMType      pcmType;    /* PCM type to support */
   int              nModes;
   USC_Modes        modes;
} USC_Option;

/* USC codec information */
typedef struct {
   const char        *name;          /* codec name */
   int                maxbitsize;    /* bitstream max frame size in bytes */
   int                nPcmTypes;     /* PCM Types tbl entries number */
   USC_PCMType       *pPcmTypesTbl;  /* supported PCMs lookup table */
   int                nRates;        /* Rate tbl entries number */
   const USC_Rates   *pRateTbl;      /* supported bitrates lookup table */
   USC_Option         params;        /* what is supported */
} USC_CodecInfo;

/* USC compressed bits */
typedef struct {
	uint8_t      *pBuffer;    /* bitstream data buffer pointer */
	int        nbytes;     /* bitstream size in byte */
	int        frametype;  /* codec specific frame type (transmission frame type) */
	int        bitrate;    /* in bps */
} USC_Bitstream;

/* USC memory banks */
typedef struct {
   char            *pMem;         /* memory buffer pointer */
   int              nbytes;       /* memory buffer length in bytes */
   int              align;        /* memory buffer alignment type */
   USC_MemType      memType;      /* memory usage type */
   USC_MemSpaceType memSpaceType; /* memory type */
} USC_MemBank;

typedef void* USC_Handle;

typedef struct _USCCodec {
   USC_MemBank*        pBanks;
   USC_Handle          hUSCCodec;
} USCCodec;

struct ipp_g729_codec_instance {
	ipp_g729_codec_instance();
	~ipp_g729_codec_instance();

	bool init(USC_Direction direction, bool annexb = false);
	void free();
    USC_Status encode(USC_PCMStream *in, USC_Bitstream *out);
    USC_Status decode(USC_Bitstream *in, USC_PCMStream *out);

private:
    /*   GetOutStreamSize()
         bitrate -  input bitrate in kbit/s,
         nBytesSrc - lenght in bytes of the input stream,
         nBytesDst - output lenght in bytes of the output stream,
     */
    USC_Status GetOutStreamSize(const USC_Option *options, int bitrate, int nbytesSrc, int *nbytesDst);
    /*   SetFrameSize()
         frameSize -  Desired frame size in bytes,
     */
    USC_Status SetFrameSize(const USC_Option *options, USC_Handle handle, int frameSize);

    //common:
    //USC_AlgType       algType;
    USC_Status GetInfoSize(Ipp32s *pSize);
	USC_Status GetInfo(USC_Handle handle, USC_CodecInfo *pInfo);
	USC_Status NumAlloc(const USC_Option *options, Ipp32s *nbanks);
	USC_Status MemAlloc(const USC_Option *options, USC_MemBank *pBanks);
	USC_Status Init(const USC_Option *options, const USC_MemBank *pBanks, USC_Handle *handle);
	USC_Status Reinit(const USC_Modes *modes, USC_Handle handle);
	USC_Status Control(const USC_Modes *modes, USC_Handle handle);

	bool info_alloc();
	bool set_encoder_pcm_type(const USC_PCMType& desPCMType);
	void set_decoder_pcm_type(const USC_PCMType& desPCMType);
	bool codec_alloc();
	bool codec_init();

    USC_CodecInfo*      m_pInfo;
	Ipp32s              m_nbanks;
	USCCodec            m_uCodec;
};

#endif //__ipp_g729_codec_instance_h__
