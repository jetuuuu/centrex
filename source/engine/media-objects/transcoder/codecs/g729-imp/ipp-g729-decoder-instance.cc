#include "ipp-g729-decoder-instance.h"

ipp_g729_decoder_instance::ipp_g729_decoder_instance() {
	m_state = USC_NotInitialized;
}

ipp_g729_decoder_instance::~ipp_g729_decoder_instance() {
}

bool ipp_g729_decoder_instance::init(bool annexB) {
	int pcmTypesNumber = 1;
	if (!m_codec.init(USC_DECODE, annexB)) {
		return false;
	}
	m_state = USC_NoError;
	return true;
}

int ipp_g729_decoder_instance::exec(uint8_t* in, uint8_t* out) {
	USC_PCMStream PCMStream;
	if (USC_NoError == m_state) {
		USC_Bitstream Bitstream;
		Bitstream.bitrate = 8000;
		Bitstream.frametype = 3;
		Bitstream.nbytes = 10; //fixed
		Bitstream.pBuffer = in;
		PCMStream.pBuffer = out;
		if (m_codec.decode(&Bitstream, &PCMStream)) {
			return 0;
		}
	}
	return PCMStream.nbytes;
}
