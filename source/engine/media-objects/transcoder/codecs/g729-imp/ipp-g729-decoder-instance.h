#ifndef __ipp_g729_decoder_instance_h__
#define __ipp_g729_decoder_instance_h__

#include "ipp-g729-codec-instance.h"

	struct ipp_g729_decoder_instance {
		ipp_g729_decoder_instance();
		~ipp_g729_decoder_instance();

		bool init(bool annexB = false);
		int exec(uint8_t* in, uint8_t* out);

	private:
		ipp_g729_codec_instance m_codec;
		USC_Status m_state;
	};

#endif //__ipp_g729_decoder_instance_h__
