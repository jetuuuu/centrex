#include "ipp-g729-encoder-instance.h"

#include <stddef.h>

#include "cms-logger.h"

ipp_g729_encoder_instance::ipp_g729_encoder_instance() {
	m_state = USC_NotInitialized;
}

ipp_g729_encoder_instance::~ipp_g729_encoder_instance() {
}

bool ipp_g729_encoder_instance::init(bool annexB) {
	m_state = USC_NoError;
	if (!m_codec.init(USC_ENCODE, annexB)) {
		return false;
	}
	m_state = USC_NoError;
	return true;
}

int ipp_g729_encoder_instance::exec(uint8_t* in, uint8_t* out) {
	USC_PCMStream PCMStream;
	USC_Bitstream Bitstream;
	if (USC_NoError == m_state) {
		PCMStream.pBuffer = in;
		Bitstream.pBuffer = out;
		if (USC_NoError != m_codec.encode(&PCMStream, &Bitstream)) {
			LOG_DEBUG_STR("Error on encode L16 stream");
			return 0;
		}
	}
	return Bitstream.nbytes;
}
