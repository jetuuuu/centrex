#ifndef _OPUS_H
#define _OPUS_H

#include <packet-buffer.h>
#include <cms-rtp.h>

#include <ipps.h>

#include "codec-processing.h"

struct opus_encoder : transcoder_processing {
	opus_encoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		// TODO:
	}
	virtual ~opus_encoder() {
	}
};

struct opus_decoder : transcoder_processing {
	opus_decoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		// TODO:
	}
	virtual ~opus_decoder() {
	}
};

#endif // _OPUS_H
