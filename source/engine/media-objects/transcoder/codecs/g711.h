#ifndef _G711_H
#define _G711_H

#include <packet-buffer.h>
#include <cms-rtp.h>

#include <ipps.h>

#include "codec-processing.h"

void decode_g711u(uint8_t* data_g711, int16_t* data, uint16_t samples) {
	IppStatus status = ippsMuLawToLin_8u16s(data_g711, data, samples);
	if (status != ippStsNoErr) {
		if (status == ippStsNullPtrErr) {
			fprintf(stderr, "ippsMuLawToLin_8u16s return ippStsNullPtrErr\n");
		} else if (status == ippStsSizeErr) {
			fprintf(stderr, "ippsMuLawToLin_8u16s return ippStsSizeErr\n");
		}
	} 
}

void encode_g711u(int16_t* data, uint8_t* data_g711, uint16_t samples) {
	IppStatus status = ippsLinToMuLaw_16s8u(data, data_g711, samples);
	if (status != ippStsNoErr) {
		if (status == ippStsNullPtrErr) {
			fprintf(stderr, "ippsLinToMuLaw_16s8u return ippStsNullPtrErr\n");
		} else if (status == ippStsSizeErr) {
			fprintf(stderr, "ippsLinToMuLaw_16s8u return ippStsSizeErr\n");
		}
	} 
}

void decode_g711a(uint8_t* data_g711, int16_t* data, uint16_t samples) {
	IppStatus status = ippsALawToLin_8u16s(data_g711, data, samples);
	if (status != ippStsNoErr) {
		if (status == ippStsNullPtrErr) {
			fprintf(stderr, "ippsALawToLin_8u16s return ippStsNullPtrErr\n");
		} else if (status == ippStsSizeErr) {
			fprintf(stderr, "ippsALawToLin_8u16s return ippStsSizeErr\n");
		}
	} 
}

void encode_g711a(int16_t* data, uint8_t* data_g711, uint16_t samples) {
	IppStatus status = ippsLinToALaw_16s8u(data, data_g711, samples);
	if (status != ippStsNoErr) {
		if (status == ippStsNullPtrErr) {
			fprintf(stderr, "ippsLinToALaw_16s8u return ippStsNullPtrErr\n");
		} else if (status == ippStsSizeErr) {
			fprintf(stderr, "ippsLinToALaw_16s8u return ippStsSizeErr\n");
		}
	} 
}

//=>L16_8000(Nms,N*16b)->enc->G711A(Nms,N*8b)=>
struct g711a_encoder : transcoder_processing {
	g711a_encoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		packet_t* out_packet = create_packet(in_packet);

		encode_g711a(reinterpret_cast<int16_t*>(&in_packet->data[sizeof(rtp_header_t)]), 
				     &out_packet->data[sizeof(rtp_header_t)], (in_packet->len - sizeof(rtp_header_t)) / 2);
		out_packet->len = sizeof(rtp_header_t) + (in_packet->len - sizeof(rtp_header_t)) / 2;
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_pcma;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
	}
	virtual ~g711a_encoder() {
	}
};

//=>G711A(Nms,N*8b)->enc->L16_8000(Nms,N*16b)=>
struct g711a_decoder : transcoder_processing {
	g711a_decoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		packet_t* out_packet = create_packet(in_packet);

		decode_g711a(&in_packet->data[sizeof(rtp_header_t)], 
			         reinterpret_cast<int16_t*>(&out_packet->data[sizeof(rtp_header_t)]), in_packet->len - sizeof(rtp_header_t));
		out_packet->len = sizeof(rtp_header_t) + (in_packet->len - sizeof(rtp_header_t)) * 2;
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_l16_8000;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
	}
	virtual ~g711a_decoder(){
	}
};

//=>L16_8000(Nms,N*16b)->enc->G711U(Nms,N*8b)=>
struct g711u_encoder : transcoder_processing {
	g711u_encoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		packet_t* out_packet = create_packet(in_packet);

		encode_g711u(reinterpret_cast<int16_t*>(&in_packet->data[sizeof(rtp_header_t)]), 
			         &out_packet->data[sizeof(rtp_header_t)], (in_packet->len - sizeof(rtp_header_t)) / 2);
		out_packet->len = sizeof(rtp_header_t) + (in_packet->len - sizeof(rtp_header_t)) / 2;
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_pcmu;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
	}
	virtual ~g711u_encoder() {
	}
};

//=>G711U(Nms,N*8b)->enc->L16_8000(Nms,N*16b)=>
struct g711u_decoder : transcoder_processing {
	g711u_decoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		packet_t* out_packet = create_packet(in_packet);

		decode_g711u(&in_packet->data[sizeof(rtp_header_t)], 
			         reinterpret_cast<int16_t*>(&out_packet->data[sizeof(rtp_header_t)]),  in_packet->len - sizeof(rtp_header_t));
		out_packet->len = sizeof(rtp_header_t) + (in_packet->len - sizeof(rtp_header_t)) * 2;
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_l16_8000;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
	}
	virtual ~g711u_decoder() {
	}
};

#endif // _G711_H
