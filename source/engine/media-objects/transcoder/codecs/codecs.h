#ifndef _CODECS_H
#define _CODECS_H

#include "codec-processing.h"
#include "g711.h"
#include "g722.h"
#include "g729.h"
#include "opus.h"

#include "codec-consts.h"

#endif // _CODECS_H