#ifndef _CODEC_PROCESSING_H
#define _CODEC_PROCESSING_H

struct transcoder_processing {
	transcoder_processing(media_object_t* parent_object) : m_parent_object(parent_object) {
	}
	virtual void transcode(packet_t* in_packet) = 0;

	struct media_object_t* get_parent_object() { return m_parent_object; }

	virtual ~transcoder_processing() {
	}

protected:
	packet_t* create_packet(packet_t* in_packet) {
		packet_pool* ppool = get_parent_object()->get_packet_buffer()->get_packet_pool();
		packet_t* ret_packet = ppool->pull_zero();
		memcpy(&ret_packet->data, &in_packet->data, sizeof(rtp_header_t));//header
		memcpy(&ret_packet->stream, (void*)&(in_packet->stream), sizeof(stream_t));
		receiver_t* recv = get_parent_object()->receiver();
        if (recv != NULL) {
            memcpy(&(ret_packet->recv), (void*)recv, sizeof(receiver_t));
        }
        return ret_packet;
	}
private:
	media_object_t* m_parent_object;
};

#endif // _CODEC_PROCESSING_H
