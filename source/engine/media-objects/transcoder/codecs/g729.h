#ifndef _G729_H
#define _G729_H

#include <packet-buffer.h>
#include <cms-rtp.h>

#include "codec-processing.h"
#include "codec-consts.h"

#include "g729-imp/ipp-g729-decoder-instance.h"
#include "g729-imp/ipp-g729-encoder-instance.h"

#define ANNEXB_YES "annexb=yes"

//#define TEST_10ms
//=>L16_8000(Nx10ms, Nx160b+12b header)->enc->G.729(Nx10ms, Nx10b+12b header)=>
struct g729_encoder : transcoder_processing {
	g729_encoder(media_object_t* parent, const std::string& format)
			: transcoder_processing(parent) {
		if (!m_encoder.init(format == ANNEXB_YES)) {
			LOG_DEBUG_STR("Can't init encoder instance.\n");
		}
#ifdef TEST_10ms
		m_curseqNumb = 0;
#endif //TEST_10ms
	}
	virtual void transcode(packet_t* in_packet) {
		//fprintf(stderr, "Enter to g729 encoder transcode method in_size=%d\n", in_packet->len);
		packet_t* out_packet = this->create_packet(in_packet);
		int out_pos = sizeof(rtp_header_t);
#ifdef TEST_10ms
		packet_t* out_packet1 = this->create_packet(in_packet);
		rtp_header_t* header1 = reinterpret_cast<rtp_header_t*>(out_packet1->data);
		int inp_pos = out_pos;
		m_encoder.exec(&in_packet->data[inp_pos], &out_packet->data[out_pos]);
		if (in_packet->len > 172) {
			m_encoder.exec(&in_packet->data[inp_pos + DEFAULT_L16_HALF_FRAME_SIZE], &out_packet1->data[out_pos]);
		}
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_g729;
		fprintf(stderr, "Current packet seqnumber=%d\n", header->seq_number);
		if (m_curseqNumb == 0) {
			m_curseqNumb = header->seq_number;
		}
		else {
			header->seq_number = ++m_curseqNumb;
		}
		out_packet->len = out_pos + 10;
		if (in_packet->len > 172) {
			header1->type = payload_type_g729;
			header1->seq_number = ++m_curseqNumb;
			header1->timestamp += 80;
			out_packet1->len = out_pos + 10;
		}

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		struct timespec tms;
		clock_gettime(CLOCK_REALTIME, &tms);
		fprintf(stderr, "Encoder id=%s: Create new packet seqnumber=%d, ptr=%p, time=%u\n", get_parent_object()->id().c_str(), header->seq_number, out_packet, tms.tv_sec * 1000 +  tms.tv_nsec / 1000000);
		pqueue->enqueue(out_packet);
		if (in_packet->len > 172) {
			fprintf(stderr, "Create new packet seqnumber=%d, ptr=%p\n", header1->seq_number, out_packet1);
			pqueue->enqueue(out_packet1);
			get_parent_object()->get_packet_buffer()->get_packet_pool()->push(out_packet1);
		}
#else //TEST_10ms
		for (int inp_pos = out_pos; inp_pos < in_packet->len; inp_pos += DEFAULT_L16_HALF_FRAME_SIZE) {
			out_pos += m_encoder.exec(&in_packet->data[inp_pos], &out_packet->data[out_pos]);
		}
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_g729;
		out_packet->len = out_pos;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
#endif //TEST_10ms
		//fprintf(stderr, "Leave g729 encoder transcode method outsize=%d\n", out_packet->len);
	}
	virtual ~g729_encoder() {
	}
private:
	ipp_g729_encoder_instance m_encoder;
#ifdef TEST_10ms
	uint16_t m_curseqNumb;
#endif //TEST_10ms
};
//(N=1-23)
//=>G.729(Nx10ms, 0-230b+12b header)->dec->L16_8000(Nx10ms, Nx160b+12b header)=>
//=>G.729(CN, 2b+12b header)->dec->L16_8000(10ms, 160b+12b header)=>
//=>NULL->PLC->L16_8000(10ms, 160b+12b header)=>
struct g729_decoder : transcoder_processing {
	g729_decoder(media_object_t* parent, const std::string& format)
			: transcoder_processing(parent) {
		if (!m_decoder.init(format == ANNEXB_YES)) {
			LOG_DEBUG_STR("Can't init decoder instance.\n");
		}
		m_prev_packet = NULL;
	}
	virtual void transcode(packet_t* in_packet) {
		int out_pos = sizeof(rtp_header_t);
		packet_t* out_packet = NULL;
		if (!in_packet) { //PLC
			if (m_prev_packet) {
				out_packet = this->create_packet(m_prev_packet);
				rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
				header->seq_number++;
				header->timestamp += 80; // sample_rate_at_ms * frame_length_ms |(8000 / 1000) * 10;
				if (m_prev_packet) {
					packet_pool* ppool = get_parent_object()->get_packet_buffer()->get_packet_pool();
					ppool->push(m_prev_packet);
				}
				m_prev_packet = this->create_packet(out_packet);
				out_pos += m_decoder.exec(NULL, &out_packet->data[out_pos]);
			}
			else {
				LOG_DEBUG_STR("Can't recover frame. Previous packets absent.\n");
				return;
			}
		}
		else {
			//TODO:variable packet size; ~(2048-12) / 16)
			if (in_packet->len > 120) {//output packet size is very long
				fprintf(stderr, "Can't decode. Very large packet, in_size=%d\n", in_packet->len);
				LOG_DEBUG("Can't decode. Very large packet, in_size=%d\n", in_packet->len);
				return;
			}
			//fprintf(stderr, "Enter to g729 decoder transcode method in_size=%d\n", in_packet->len);
			out_packet = this->create_packet(in_packet);
			for(int inp_pos = out_pos; inp_pos < in_packet->len; inp_pos += 10) {
				//TODO:check null-length packet
				out_pos += m_decoder.exec(&in_packet->data[inp_pos], &out_packet->data[out_pos]);
			}
			if (m_prev_packet) {
				packet_pool* ppool = get_parent_object()->get_packet_buffer()->get_packet_pool();
				ppool->push(m_prev_packet);
			}
			m_prev_packet = this->create_packet(in_packet);
		}
		out_packet->len = out_pos;
		rtp_header_t* header = reinterpret_cast<rtp_header_t*>(out_packet->data);
		header->type = payload_type_l16_8000;

		packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
		pqueue->enqueue(out_packet);
		//fprintf(stderr, "Leave g729 decoder transcode method outsize=%d\n", out_packet->len);
	}
	virtual ~g729_decoder() {
		if (m_prev_packet) {
			packet_queue* pqueue = get_parent_object()->get_packet_buffer()->get_packet_queue();
			pqueue->enqueue(m_prev_packet);
		}
	}
private:
	ipp_g729_decoder_instance m_decoder;
	packet_t* m_prev_packet;
};

#endif // _G729_H
