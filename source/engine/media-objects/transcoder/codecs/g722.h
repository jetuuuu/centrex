#ifndef _G722_H
#define _G722_H

#include <packet-buffer.h>
#include <cms-rtp.h>

#include <ipps.h>

#include "codec-processing.h"

struct g722_encoder : transcoder_processing {
	g722_encoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		// TODO:
	}
	virtual ~g722_encoder() {
	}
};

struct g722_decoder : transcoder_processing {
	g722_decoder(media_object_t* parent)
			: transcoder_processing(parent) {
	}
	virtual void transcode(packet_t* in_packet) {
		// TODO:
	}
	virtual ~g722_decoder() {
	}
};

#endif // _G722_H
