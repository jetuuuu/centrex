#ifndef _TRANSCODER_H
#define _TRANSCODER_H

#include <media-obj.h>
#include <packet-buffer.h>
#include <mediaserver.pb.h>

#include <codecs.h>

struct transcoder : media_object_t {
public:
	transcoder(const uint32_t& mg, packet_buffer* buffer, const mediaserver::Codec& in_codec, const mediaserver::Codec& out_codec) : 
		media_object_t(mg, buffer),
		m_in_codec(in_codec),
		m_out_codec(out_codec) {
		const std::string& in_codec_name = in_codec.name();
		const std::string& in_codec_format = in_codec.format();
		const std::string& out_codec_name = out_codec.name();
		const std::string& out_codec_format = out_codec.format();
		if (in_codec_name.compare(CODEC_NAME_L16_8000) == 0) {
			// encode
			if (out_codec_name.compare(CODEC_NAME_PCMA) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g711a_encoder(this));
			} else if (out_codec_name.compare(CODEC_NAME_PCMU) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g711u_encoder(this));
			} else if (out_codec_name.compare(CODEC_NAME_G722) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g722_encoder(this));
			} else if (out_codec_name.compare(CODEC_NAME_G729) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g729_encoder(this, out_codec_format));
			} else if (out_codec_name.compare(CODEC_NAME_OPUS) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new opus_encoder(this));
			} else {
				fprintf(stderr, "Undefined out_codec name \"%s\"\n", out_codec_name.c_str());
			}
		} else if (out_codec_name.compare(CODEC_NAME_L16_8000) == 0) {
			// decode
			if (in_codec_name.compare(CODEC_NAME_PCMA) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g711a_decoder(this));
			} else if (in_codec_name.compare(CODEC_NAME_PCMU) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g711u_decoder(this));
			} else if (in_codec_name.compare(CODEC_NAME_G722) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g722_decoder(this));
			} else if (in_codec_name.compare(CODEC_NAME_G729) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new g729_decoder(this, in_codec_format));
			} else if (in_codec_name.compare(CODEC_NAME_OPUS) == 0) {
				m_processing = reinterpret_cast<transcoder_processing*>(new opus_decoder(this));
			} else {
				fprintf(stderr, "Undefined in_codec name \"%s\"\n", in_codec_name.c_str());
			}
		} else {
			fprintf(stderr, "Can't support processing from \"%s\" to \"%s\"\n", in_codec_name.c_str(), out_codec_name.c_str());
		}
	}

	virtual ~transcoder() {
		if (m_processing != NULL) {
			delete m_processing;
		}
	}

	virtual bool process_packet(packet_t* packet) {
        if (packet->rtcp) {
    		packet_pool* ppool = get_packet_buffer()->get_packet_pool();
    		packet_t* out_packet = ppool->pull_zero();
    		memcpy(&out_packet->data, &packet->data, packet->len);//header
    		memcpy(&out_packet->stream, (void*)&(packet->stream), sizeof(stream_t));
    		out_packet->rtcp = packet->rtcp;
    		receiver_t* recv = receiver();
            if (recv != NULL) {
                memcpy(&(out_packet->recv), (void*)recv, sizeof(receiver_t));
            }
    		packet_queue* pqueue = get_packet_buffer()->get_packet_queue();
    		pqueue->enqueue(out_packet);
        } else if (m_processing != NULL) {
			m_processing->transcode(packet);
        }
		return false;
	}

private:
	const mediaserver::Codec m_in_codec;
	const mediaserver::Codec m_out_codec;
	transcoder_processing* m_processing;
};

#endif // _TRANSCODER_H 
