#ifndef _EMPTY_OBJECT_H
#define _EMPTY_OBJECT_H

#include <media-obj.h>
#include <packet-buffer.h>

struct empty_object : media_object_t {
public:
	empty_object(const uint32_t& mg, packet_buffer* buffer) : media_object_t(mg, buffer) {
	}

	virtual ~empty_object() {
	}

	virtual bool process_packet(packet_t* packet) {
		receiver_t* recv = receiver();
        if (recv != NULL) {
            memcpy(&(packet->recv), (void*)recv, sizeof(receiver_t));
        } else {
        	printf("empty_object:process_packet receiver is NULL for object %d\n", id());
        	return false;
        }
		return true;
	}
};

#endif // _EMPTY_OBJECT_H 