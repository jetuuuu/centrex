#ifndef _ENGINE_H
#define _ENGINE_H

#include <ifaddrs.h>

#include <cms.h>
#include <cms-obj-pool.h>
#include <cms-net.h>
#include <cms-logger.h>

#include "processor.h"
#include "packet-buffer.h"
#include "cluster.h"
#include "media-objects.h"

void get_available_cores(std::list<uint32_t> &list) {
    cpu_set_t cores;
    int rc = sched_getaffinity(0, sizeof(cpu_set_t), &cores);
    if (rc) {
        return;
    }
    for (int i = 0; i < CPU_SETSIZE; ++i) {
        if (CPU_ISSET(i, &cores)) {
            if (i == 15) {
                fprintf(stderr, "Warning: Using only 15 cores.");
                break;
            }
            list.push_back(i);
        }
    }
}

class engine {
public:
    engine(const uint32_t& nodeId, struct event_base* base) : m_cluster(NULL), m_node_id(nodeId) {
        LOG_DEBUG("WAN IP=%s", net::get_eth_ipv4().c_str());
        
        mzones_list packets_mzones;
        rte_memzone_t packets_mzone;
        LOG_DEBUG("sizeof(uintptr_t)=%d", (int)sizeof(uintptr_t));
        packets_mzone.length = 10 * 1024 * 1024 * sizeof(uintptr_t);
        packets_mzone.addr = (uintptr_t)malloc(packets_mzone.length);
        if (!packets_mzone.addr) {
            fprintf(stderr, "packets_mzone.addr is NULL\n");
            return;
        }
        packets_mzones.push_back(packets_mzone);
        m_packet_pool = memory_object_pool<packet_t>::instance(packets_mzones);
        if (m_packet_pool == NULL) {
            fprintf(stderr, "Packet Pool is NULL\n");
        }

        LOG_DEBUG("packets count=%d free_count=%d used_count=%d", (int)m_packet_pool->get_count(), (int)m_packet_pool->get_free_count(),(int)m_packet_pool->get_used_count());
        LOG_DEBUG("memory mem=%d free_mem=%d get_used_mem=%d", (int)m_packet_pool->get_mem(), (int)m_packet_pool->get_free_mem(), (int)m_packet_pool->get_used_mem());

        mzones_list queue_mzones;
        rte_memzone_t queue_mzone;
        queue_mzone.length = 512 * 1024 * sizeof(uintptr_t);
        queue_mzone.addr = (uintptr_t)malloc(queue_mzone.length);
        if (!queue_mzone.addr) {
            fprintf(stderr, "queue_mzone.addr is NULL\n");
            return;
        }
        queue_mzones.push_back(queue_mzone);
        m_packet_queue = new packet_queue(queue_mzones);
        if (m_packet_queue == NULL) {
            fprintf(stderr, "m_packet_queue is NULL\n");
            return;
        }

        std::list<uint32_t> cores, buff_core_list, proc_core_list;
        get_available_cores(cores);
        LOG_DEBUG("Available cores=%d", (int)cores.size());

        if (cores.size() > 1) {
            cores.pop_front(); // одно ядро под libevent tcp+udp
        } else {
            LOG_DEBUG_STR("Warning: only one Core detected!");
        }
        proc_core_list.assign(cores.begin(), cores.end()); // остальные ядра процессорам

        m_buffer = new packet_buffer(base, m_packet_pool, m_packet_queue);
        m_objects = new media_objects(nodeId, m_buffer);
        m_processor = new processor(m_buffer, proc_core_list, m_objects);
    }

    virtual ~engine() {
        delete m_cluster;
        delete m_processor;
        delete m_buffer;

        delete m_packet_pool;
        delete m_packet_queue;
        delete m_objects;
    }

    uint32_t const id() const { return m_node_id; }

    void init_cluster(const uint32_t& node_id, const std::string& mcast, const uint16_t& cluster_tcp_port) {
        m_cluster = new cluster(m_objects, node_id, mcast, cluster_tcp_port);
    }

    uint32_t create_endpoint(const mediaserver::Media& media, struct evbuffer* output) {
        LOG_DEBUG_STR("call create_endpoint");
        return m_objects->create_endpoint(media, output);
    }

    uint32_t create_video_mixer() {
        LOG_DEBUG_STR("call create_video_mixer");
        return m_objects->create_video_mixer();
    }

    void destroy_object(const uint32_t& id) {
        LOG_DEBUG("call destroy_object with id=\"%d\"", id);
        m_objects->destroy_object(id);
    }

    mediaserver::Media get_endpoint_media(const uint32_t& id) {
        LOG_DEBUG("call get_endpoint_media with id=\"%d\"", id);  
        
        endpoint* ep = reinterpret_cast<endpoint*>(m_objects->get_object(id));
        if (ep == NULL) {
            LOG_DEBUG("can't find endpoint with id=\"%d\"", id);
            return mediaserver::Media();
        }
        return ep->local_media();
    }

    void update_endpoint_media(const uint32_t& id, const mediaserver::Media& new_media) {
        LOG_DEBUG("call update_endpoint_media with id=\"%d\"", id);

        endpoint* ep = reinterpret_cast<endpoint*>(m_objects->get_object(id));
        if (ep == NULL) {
            LOG_DEBUG("can't find endpoint with id=\"%d\"", id);
            return;
        }
        ep->update_remote_media(new_media);
    }

    void send_dtmf(const uint32_t& id, const std::string& dtmf) {
        LOG_DEBUG("call send_dtmf id=\"%d\" dtmf=\"%s\"", id, dtmf.c_str());

        endpoint* ep = reinterpret_cast<endpoint*>(m_objects->get_object(id));
        if (ep == NULL) {
            LOG_DEBUG("can't find endpoint with id=\"%d\"", id);
            return;
        }
        uint8_t dtmf_code = 0;
        if (dtmf.find_first_not_of("0123456789") == std::string::npos) {
            dtmf_code = atoi(dtmf.c_str()); 
        } else if (dtmf == "*") {
            dtmf_code = 10;
        } else if (dtmf == "#") {
            dtmf_code = 11;
        } else {
            LOG_DEBUG("Undefined dtmf =\"%s\"", dtmf.c_str());
            return;
        }

        ep->send_dtmf(dtmf_code);
    }

    void set_receiver(const uint32_t& id, const uint32_t& recv_id) {
        LOG_DEBUG("call set_receiver with obj_id=\"%d\" recv_id=\"%d\"", id, recv_id);

        media_object_t* obj = m_objects->get_object(id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", id);
            return;
        }
        media_object_t* obj_recv = m_objects->get_object(recv_id);
        if (obj_recv == NULL) {
            LOG_DEBUG("can't find receiver object with id=\"%d\"", recv_id);
            return;
        }
        
        receiver_t* recv = new receiver_t();
        recv->obj.mg = this->id();
        recv->obj.id = recv_id;
        recv->obj.tbl_oid = obj_recv->tbl_oid();
        obj->set_receiver(recv);
    }

    void set_receiver(const uint32_t& id, const uint32_t& recv_mg, const uint32_t& recv_id,
                                          const uint32_t& recv_ip, const uint32_t& recv_port) {
        LOG_DEBUG("call set_receiver with obj_id=\"%d\" recv=\"%d:%d\" addr=%d:%d", id, recv_mg, recv_id, recv_ip, recv_port);

        media_object_t* obj = m_objects->get_object(id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", id);
            return;
        }                
        receiver_t* recv = new receiver_t();
        recv->obj.mg = this->id();
        recv->obj.id = recv_id;
        recv->addr.sin_addr.s_addr = recv_ip;
        recv->addr.sin_port = htons(recv_port);
        recv->addr.sin_family = AF_INET;
        recv->send = true;

        obj->set_receiver(recv);
    }

    void reset_receiver(const uint32_t& id) {
        LOG_DEBUG("call reset_receiver with obj_id=\"%d\"", id);

        media_object_t* obj = m_objects->get_object(id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", id);
            return;
        }
        obj->set_receiver(NULL);
    }

    uint32_t create_player(const std::string& file_path) {
        LOG_DEBUG("call create_player file=\"%s\"", file_path.c_str());
        return m_objects->create_player(file_path);
    }

    uint32_t create_recorder(const std::string& file_path) {
        LOG_DEBUG("call create_recorder file=\"%s\"", file_path.c_str());
        return m_objects->create_recorder(file_path);
    }

    uint32_t create_transcoder(const mediaserver::Codec& in_codec, const mediaserver::Codec& out_codec) {
        LOG_DEBUG("call create_transcoder in=\"%s\" out=\"%s\"", in_codec.name().c_str(), out_codec.name().c_str());
        return m_objects->create_transcoder(in_codec, out_codec);
    }

    uint32_t create_srtp(const std::string& crypto, const bool& decrypt) {
        LOG_DEBUG("call create_srtp crypto=\"%s\" decrypt=%d", crypto.c_str(), decrypt);
        return m_objects->create_srtp(crypto, decrypt);
    }

    uint32_t create_mixer() {
        LOG_DEBUG_STR("call create_mixer");
        return m_objects->create_mixer();
    }
    void attach_to_mixer(const uint32_t& mixer_id, const uint32_t& obj_id) {
        LOG_DEBUG("call attach_to_mixer with mixer_id=\"%d\" obj_id=\"%d\"", mixer_id, obj_id);

        media_object_t* obj = m_objects->get_object(mixer_id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", obj_id);
            return;
        }
        mixer* m = dynamic_cast<mixer*>(obj);
        if (m == NULL) {
            LOG_DEBUG("can't cast media object with id=\"%d\" to mixer", mixer_id);
            return;   
        }

        // TODO: надо что-то делать с obj_id
    }
    void detach_from_mixer(const uint32_t& mixer_id, const uint32_t& obj_id) {
        LOG_DEBUG("call detach_from_mixer with mixer_id=\"%d\" obj_id=\"%d\"", mixer_id, obj_id);

        media_object_t* obj = m_objects->get_object(mixer_id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", obj_id);
            return;
        }
        mixer* m = dynamic_cast<mixer*>(obj);
        if (m == NULL) {
            LOG_DEBUG("can't cast media object with id=\"%d\" to mixer", mixer_id);
            return;   
        }

        // TODO: надо что-то делать с obj_id
    }


    uint32_t create_simple_mixer() {
        LOG_DEBUG_STR("call create_simple_mixer");
        return m_objects->create_simple_mixer();
    }
    // TODO: добавить метод при добавлении в качестве ресивера адреса
    void attach_stream_to_simple_mixer(const uint32_t& obj_id, const stream_t& stream, const uint32_t& recv_id) {
        LOG_DEBUG("call attach_stream_to_simple_mixer with obj_id=\"%d\" stream_id=\"%d:%d\" recv_id=\"%d\"", obj_id, stream.obj.mg, stream.obj.id, recv_id);

        media_object_t* obj = m_objects->get_object(obj_id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", obj_id);
            return;
        }
        simple_mixer* mixer = dynamic_cast<simple_mixer*>(obj);
        if (mixer == NULL) {
            LOG_DEBUG("can't cast media object with id=\"%d\" to simple_mixer", obj_id);
            return;   
        }
        media_object_t* obj_recv = m_objects->get_object(recv_id);
        
        receiver_t* recv = new receiver_t();
        recv->obj.id = recv_id;
        mixer->attach_stream(stream, recv);
    } 
    void detach_stream_from_simple_mixer(const uint32_t& obj_id, const stream_t& stream) {
        LOG_DEBUG("call detach_stream_from_simple_mixer with obj_id=\"%d\" stream_id=\"%d:%d\"", obj_id, stream.obj.mg, stream.obj.id);

        media_object_t* obj = m_objects->get_object(obj_id);
        if (obj == NULL) {
            LOG_DEBUG("can't find media object with id=\"%d\"", obj_id);
            return;
        }
        simple_mixer* mixer = dynamic_cast<simple_mixer*>(obj);
        if (mixer == NULL) {
            LOG_DEBUG("can't cast media object with id=\"%d\" to simple_mixer", obj_id);
            return;   
        }
        mixer->detach_stream(stream);
    }

    uint32_t create_empty_object() {
        LOG_DEBUG_STR("call create_empty_object");
        return m_objects->create_empty_object();
    }
    

private:
    uint32_t const m_node_id;

    packet_pool* m_packet_pool;
    packet_queue* m_packet_queue;
    packet_buffer* m_buffer;

    media_objects* m_objects;

    processor* m_processor;
    
    cluster* m_cluster;
};

#endif // _ENGINE_H
