#ifndef _PROCESSOR_H
#define _PROCESSOR_H

#include <stdio.h>
#include <unistd.h>

#include <cms.h>
#include <cms-logger.h>
#include <cms-time.h>
#include <packet-buffer.h>
#include <media-objects.h>

enum processor_state_t {
    PROCESSOR_INACTIVE,
    PROCESSOR_SHUTTING_DOWN,
    PROCESSOR_ACTIVE
};

struct processor {
public:
    class thread : public std::thread {
    public:
        thread(processor* processor, const uint8_t& core, packet_buffer* buffer, media_objects* objects) : 
            std::thread(&processor::thread::run, this),
            m_processor(processor), 
            m_core(core),
            m_buffer(buffer),
            m_objects(objects) {}

        int run() {
            LOG_DEBUG_STR("processor thread start...");

            do {
                sched_yield();
            } while (likely(m_processor->m_state == PROCESSOR_INACTIVE));
            packet_queue* pqueue = m_buffer->get_packet_queue();
            packet_pool* ppool = m_buffer->get_packet_pool();
            do {
                if (!pqueue->empty()) {
                    packet_t* packet = NULL;
                    if (!pqueue->dequeue(&packet)) {
                        continue;
                    }
                    if (packet == NULL) {
                        continue;
                    }

                    if (packet->recv.obj.id > 0) {
                        if (process_packet(packet)) {
                            pqueue->enqueue(packet);
                            continue;
                        } 
                    } else {
                        // printf("process packet: packet->recv.obj.id is 0\n"); 
                    }
                    //rtp_header_t* header = reinterpret_cast<rtp_header_t*>(packet->data);
            		//fprintf(stderr, "Packet destroyed seqnumber=%d, ptr=%p\n", header->seq_number, packet);
                    ppool->push(packet);
                    // printf("blocks count=%d free_count=%d used_count=%d\n", (int)ppool->get_count(), (int)ppool->get_free_count(),(int)ppool->get_used_count());

                } else {
                    usleep(10000);
                    // sched_yield();
                }
            } while (likely(m_processor->m_state == PROCESSOR_ACTIVE));

            LOG_DEBUG_STR("processor thread stop...");
            return 0;
        }

    private:
        bool process_packet(packet_t* packet) {
            // printf("processor process_packet %s\n", packet->recv.receiver);

            if (!packet->recv.send) {
                uint32_t oid = packet->recv.obj.tbl_oid;
                media_object_t* obj = m_objects->get_object_from_table(oid);
                if (obj != NULL) {
                    if (m_objects->is_mark_del(oid)) {
                        return false;
                    }
                    m_objects->set_busy(oid, m_core);
                    bool res = obj->process_packet(packet);
                    m_objects->reset_busy(oid, m_core);
                    return res;
                }
            } else {
                // сразу посылаем пакет по указанному адресу в ресивере
                /*
                printf("Cluster! Send RTP to %s\n", packet->recv.receiver);
                cluster_packet_t cluster_packet;
                memset(&cluster_packet, 0, sizeof(cluster_packet_t));
                cluster_packet.udp_len = packet->len;
                cluster_packet.recv_len = packet->recv.recv_len;
                memcpy(&cluster_packet.data, packet->data, packet->len);
                memcpy(&cluster_packet.data[packet->len], packet->recv.receiver, packet->recv.recv_len);

                struct sockaddr_in cluster_addr = packet->recv.addr;
                cluster_addr.sin_port = packet->rtcp ? htons(20001) : htons(20000);

                const cluster_transport_t& cluster_transport = m_buffer->get_cluster_transport();
                const evutil_socket_t& cluster_socket = packet->rtcp ? const_cast<evutil_socket_t&>(cluster_transport.rtcp_socket_fd) :
                                                                               const_cast<evutil_socket_t&>(cluster_transport.rtp_socket_fd);
                //rtp_header_t* header = reinterpret_cast<rtp_header_t*>(packet->data);
                //fprintf(stderr, "Send to packet seqNumber=%d, ptr=%p\n", header->seq_number, packet);
                //fprintf(stderr, "Send to addr%d:%d\n", cluster_addr.sin_addr, cluster_addr.sin_port);
                sendto(cluster_socket, &cluster_packet, sizeof(cluster_packet_t), 0, (struct sockaddr*) &cluster_addr, sizeof(cluster_addr));
                */
            }

            return false;
        }

        bool receiver_is_ready(packet_t* packet) {
            bool res = true;
            endpoint* e = reinterpret_cast<endpoint*>(m_objects->get_object(packet->recv.obj.id));
            if (e != NULL) {
                if (packet->len > 0) {
                    res = e->get_remote_rtp_socket() != 0;
                } else {
                    res = false;
                }
            }
            return res;
        }

    private:
        processor* const m_processor;
        packet_buffer* const m_buffer;
        media_objects* const m_objects;
        const uint8_t m_core;
    };

public:
    processor(const processor&) = delete;
    processor& operator=(const processor&) = delete;

	processor(packet_buffer* buffer, const std::list<uint32_t>& core_list, media_objects* objects) 
        : m_state(PROCESSOR_INACTIVE) {
        if (core_list.size() > 0) {
            for (auto& i : core_list) {
                cpu_set_t set;
                CPU_ZERO(&set);
                CPU_SET(i, &set);
                LOG_DEBUG("processor core=%d", i);
                processor::thread* th = new processor::thread(this, i, buffer, objects);
                m_th_list.push_back(th);
                pthread_setaffinity_np(th->native_handle(), sizeof(cpu_set_t), &set);    
            }
        } else {
            processor::thread* th = new processor::thread(this, 0, buffer, objects);
            m_th_list.push_back(th);
            LOG_DEBUG_STR("processor core=default");
        }
        m_state = PROCESSOR_ACTIVE;
    }
	~processor() {
        LOG_DEBUG_STR("start ~processor");
        m_state = PROCESSOR_SHUTTING_DOWN;
        for (auto& th : m_th_list) {
            th->join();
        }

        for (auto& th : m_th_list) {
            delete th;
        }
        LOG_DEBUG_STR("end ~processor");
    }

public:
    volatile processor_state_t m_state;

private:
    std::list<processor::thread*> m_th_list;
};

#endif // _PROCESSOR_H
