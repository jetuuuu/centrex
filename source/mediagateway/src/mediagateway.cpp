#include <cms.h>

#include <signal.h>
#include <unistd.h>

#include <engine.h>
#include <cms-net.h>
#include <cms-logger.h>

#include <tcp-processing.h>

static const char*      MCAST_ARRD = "239.18.18.1:10001";
static const uint32_t   NODE_ID = 1;

static struct event_base* base;
static void signal_handler(int signum) {
    if (signum == SIGTERM || signum == SIGINT) {
        event_base_loopexit(base, NULL);
    }
    return;
}

int main (int argc, char const *argv[]) {
    fprintf(stderr, "Start MediaServer...\n");
    LOG_INIT;
    LOG_DEBUG_STR("******************** Start ********************");
    GOOGLE_PROTOBUF_VERIFY_VERSION;
    LOG_DEBUG_STR("Start MediaGateway...");
    std::string mcast_addr = MCAST_ARRD;
    uint32_t node_id = NODE_ID;
    if (argc > 1) {
        node_id = atoi(argv[1]);
        if (argc > 2) {
            mcast_addr = std::string(argv[2]);
        }
    }
    LOG_DEBUG("nodeId=%d", node_id);
    LOG_DEBUG("mcastAddr=%s", mcast_addr.c_str());
    LOG_DEBUG("PID=%d", getpid());

    base = NULL;
    signal(SIGTERM, signal_handler);

    // init libevent
    base = event_base_new();
    if (!base) {
        LOG_DEBUG_STR("Couldn't open event base");
        return 1;
    }

    engine* en = new engine(node_id, base);

    struct sockaddr_in sin;
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(0);
    sin.sin_port = 0;

    struct evconnlistener* listener = evconnlistener_new_bind(base, tcp_accept_connection_callback, en, LEV_OPT_CLOSE_ON_FREE|LEV_OPT_REUSEABLE, -1, (struct sockaddr*)&sin, sizeof(sin));
    if (!listener) {
        LOG_DEBUG_STR("Couldn't create TCP listener");
        return 1;
    }

    evutil_socket_t socket = evconnlistener_get_fd(listener);
    struct sockaddr_in addr;
    socklen_t len = sizeof(addr);
    if (getsockname(socket, (struct sockaddr*)&addr, &len) != -1) {
        LOG_DEBUG("cluster TCP port = %d", ntohs(addr.sin_port));
        en->init_cluster(node_id, mcast_addr, ntohs(addr.sin_port));
        evconnlistener_set_error_cb(listener, tcp_accept_error_callback);

        event_base_loop(base, EVLOOP_NO_EXIT_ON_EMPTY);
    } else {
        LOG_DEBUG_STR("getsockname error");
    }

    delete en;

    google::protobuf::ShutdownProtobufLibrary();

    LOG_DEBUG_STR("Stop MediaGateway...");
    LOG_DEBUG_STR("******************** Stop ********************");
    sleep(2);
    LOG_DESTROY;

    fprintf(stderr, "Stop MediaGateway...\n");
    return 0;
}

