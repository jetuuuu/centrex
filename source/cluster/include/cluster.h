#ifndef _CLUSTER_H
#define _CLUSTER_H

#include <stdlib.h>

#include <cms.h>
#include <cms-logger.h>
#include <cms-net.h>
#include <mediaserver.pb.h>
#include <media-objects.h>

#include <assert.h>

enum cluster_state_t {
    CLUSTER_INACTIVE,
    CLUSTER_SHUTTING_DOWN,
    CLUSTER_ACTIVE
};

struct cluster {
public:
    class thread : public std::thread {
    public:
        thread(cluster* cluster, media_objects* objects, const uint32_t& node_id, const std::string& mcast, const uint16_t& cluster_tcp_port) : 
            std::thread(&cluster::thread::run, this),
            m_cluster(cluster), 
            m_objects(objects),
            m_node_id(node_id),
            m_mcast(mcast),
            m_cluster_tcp_port(cluster_tcp_port) {}

        int run() {
            LOG_DEBUG_STR("cluster thread start...");

            do {
                sched_yield();
            } while (likely(m_cluster->m_state == CLUSTER_INACTIVE));

            std::string ip = net::get_eth_ipv4();

            int sock = socket(AF_INET, SOCK_DGRAM, 0);

            int len = m_mcast.find(":");
            std::string mcast_ip = m_mcast.substr(0, len);
            uint16_t mcast_port = atoi(m_mcast.substr(len + 1, m_mcast.length() - len - 1).c_str());
            struct sockaddr_in addr;
            memset(&addr, 0, sizeof(addr));
            addr.sin_family = AF_INET;
            addr.sin_port = htons(mcast_port);
            addr.sin_addr.s_addr = inet_addr(mcast_ip.c_str());

    		do {
    			mediaserver::MediaServerMsg msg;
    			msg.set_mg(m_node_id);
    			msg.set_numobjects(m_objects->size());
                msg.set_ip(inet_addr(ip.c_str()));
                msg.set_tcpclusterport(m_cluster_tcp_port);
                msg.set_udpclusterport(20000);

    			double loadavg[3];
    			if (-1 != getloadavg(loadavg, 3)) {
    				msg.set_cpuusage(loadavg[0] * 100);
    			} else {
    				msg.set_cpuusage(100);
    			}

        		std::string result = msg.SerializeAsString();
                int cnt = sendto(sock, result.c_str(), result.length(), 0, (struct sockaddr *) &addr, sizeof(addr));
                if (cnt == -1) {
                    LOG_DEBUG_STR("Can't send mcast data");
                }
                // printf("send mcast len=%d\n", cnt);
                sleep(1);
            } while (likely(m_cluster->m_state == CLUSTER_ACTIVE));

            LOG_DEBUG_STR("cluster thread stop...");
            return 0;
        }

    private:
        cluster* const m_cluster;
        media_objects* const m_objects;
        uint32_t const m_node_id;
        std::string const m_mcast;
        uint16_t const m_cluster_tcp_port;
    };

public:
    cluster(const cluster&) = delete;
    cluster& operator=(const cluster&) = delete;

	cluster(media_objects* objects, const uint32_t& node_id, const std::string& mcast, const uint16_t& cluster_tcp_port)
        : m_state(CLUSTER_INACTIVE) {
        m_th = new cluster::thread(this, objects, node_id, mcast, cluster_tcp_port);
        cpu_set_t set;
        CPU_ZERO(&set);
        CPU_SET(0, &set);
        pthread_setaffinity_np(m_th->native_handle(), sizeof(cpu_set_t), &set); 
        LOG_DEBUG("cluster core=%d", 0);
        m_state = CLUSTER_ACTIVE;
    }
	~cluster() {
        LOG_DEBUG_STR("start ~cluster");
        m_state = CLUSTER_SHUTTING_DOWN;
        m_th->join();
        delete m_th;
        LOG_DEBUG_STR("end ~cluster");
    }

public:
    volatile cluster_state_t m_state;

private:
    cluster::thread* m_th;
};

#endif // _CLUSTER_H